GitLab Metrics
==============

This is the metrics harvester and reporting for GitLab Metrics that are used to feed into end of PI Quantitative Metrics reporting.

The Harvester is deployed on the STFC cluster in the metrics Namespace, and the report generation is manually triggered from the Pipelines Pages step - https://gitlab.com/ska-telescope/sdi/ska-cicd-gitlab-metrics/-/pipelines/ .

The last generated report is published at https://ska-telescope.gitlab.io/sdi/ska-cicd-gitlab-metrics/ .
