#!/usr/bin/env python3

from datetime import datetime, timezone, timedelta
import getopt
import sys

selected_pi = 25
try:
    optlist, args = getopt.getopt(sys.argv[1:], 's:p:')
except getopt.GetoptError as err:
    print(err)
    sys.exit(1)

wanted = False
for o, a in optlist:
    if '-s' == o:
        selected_pi = int(a)
    elif '-p' == o:
        wanted = a

# PI generator
first_pi = 1
select_pis = 5 + 1

this_pi = datetime.strptime("2018-12-12", "%Y-%m-%d").replace(tzinfo=timezone.utc)
the_pis = []
last_start = ""
for pi in range(first_pi, selected_pi + 1):
    end_date = this_pi + timedelta(days=(13 * 7) + 1)
    if pi == 10:
        end_date = end_date - timedelta(days=3)
        this_pi = this_pi - timedelta(days=2)
    the_pis.append(
        {
            "pi": "PI{}".format(pi),
            "id": pi,
            "start": this_pi.strftime("%A, %Y-%m-%d"),
            "finish": (end_date - timedelta(days=1)).strftime("%A, %Y-%m-%d"),
        }
    )
    last_start = this_pi
    this_pi = end_date

for pi in the_pis:
    if not wanted or pi['pi'] == wanted.upper():
        print("* [{pi} (start {start}, end {finish})](./{link}/)".format(pi=pi['pi'], link=pi['pi'].lower(), start=pi['start'], finish=pi['finish']))
