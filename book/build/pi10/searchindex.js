Search.setIndex({docnames:["PIQualityMetricsBranches","PIQualityMetricsLicenses","PIQualityMetricsMergeRequests","PIQualityMetricsPipelines","PIQualityMetricsProjects","overview"],envversion:{"sphinx.domains.c":2,"sphinx.domains.changeset":1,"sphinx.domains.citation":1,"sphinx.domains.cpp":3,"sphinx.domains.index":1,"sphinx.domains.javascript":2,"sphinx.domains.math":2,"sphinx.domains.python":2,"sphinx.domains.rst":2,"sphinx.domains.std":2,"sphinx.ext.intersphinx":1,"sphinxcontrib.bibtex":7,sphinx:56},filenames:["PIQualityMetricsBranches.ipynb","PIQualityMetricsLicenses.ipynb","PIQualityMetricsMergeRequests.ipynb","PIQualityMetricsPipelines.ipynb","PIQualityMetricsProjects.ipynb","overview.md"],objects:{},objnames:{},objtypes:{},terms:{"000000":0,"00z":[0,1,2,3,4],"017":3,"018000":2,"01t00":[1,3],"020408":0,"044000":4,"046000":4,"055":3,"062000":[0,2],"064516":0,"097":3,"100":[0,2],"102276658":[0,2],"103":4,"103548498":[0,2],"103963940":2,"104024425":[0,2],"1041":0,"104195708":[0,2],"1042":0,"1045":0,"105":0,"107":4,"10749136":[2,3,4],"108":4,"110":2,"116000":4,"124":0,"12488466":0,"128":2,"12828461":[3,4],"129":0,"130000":[0,2],"13372840":[3,4],"13540781":[3,4],"137":2,"139":2,"142":0,"14333804":[3,4],"14401284":[0,2],"14409923":0,"14469506":0,"14483552":[0,2],"14528374":2,"1456":4,"1458":4,"14811354":0,"14822203":0,"14848025":[0,2],"15088717":0,"15124978":0,"153":0,"1541038":3,"15512400":0,"15564271":[0,2],"156":2,"15615193":[0,2],"15723860":0,"158":0,"159":2,"15t23":[0,1,2,3,4],"162000":4,"164":2,"16444104":0,"165":[3,4],"166":[3,4],"167":[3,4],"168":[0,3,4],"169":[3,4],"16px":[1,2,3],"170":[0,3,4],"171":0,"172":0,"175":0,"17509042":4,"17511993":4,"17512152":4,"17606502":4,"17606533":[0,4],"177":2,"178":0,"17807228":0,"17t00":[0,1,2,3,4],"184":2,"185":2,"18934144":0,"18t23":1,"2008":[1,3,4],"2018":4,"2019":4,"2020":[0,2,4],"2021":[0,1,2,3,4],"203":0,"2087":0,"21141379":2,"212":2,"214":2,"221":2,"22174364":2,"22181645":2,"22182258":2,"22204909":2,"2229":0,"2232":0,"2239":0,"226000":4,"22876071":2,"2291":0,"2294":0,"22944835":[0,2],"2296":0,"231000":2,"233":[0,2],"2343":4,"235":0,"236000":4,"24337273":0,"24580625":2,"246":2,"2527":0,"2547":4,"25588125":[0,2,3,4],"256000":2,"258":4,"259":0,"26389323":[3,4],"26533391":4,"26543":3,"26595478":0,"26790066":4,"268":0,"2685":0,"26972006":4,"270":2,"271":2,"272":2,"273":2,"274":2,"27416705":[3,4],"27423354":4,"2762":0,"2769":0,"285714":0,"2894":0,"2nd":2,"306":3,"310":0,"314":2,"316":2,"317":2,"331":0,"335":0,"343":[0,2],"357000":2,"357_make_tangogql_pass_attribute_names_low":0,"361":0,"364_fix_ci":0,"365":[0,2],"36686":3,"382":0,"385":0,"3876":4,"392000":[0,2],"395_tmcspsubarrayleadnode_fix":0,"396000":4,"401":0,"427000":4,"437":4,"437000":0,"44158857":0,"443000":2,"44422":3,"444444":0,"44534210":0,"44638104":0,"457831":0,"459":2,"463":2,"473":2,"482000":4,"484":[0,2],"492":2,"495050":3,"496":3,"501000":4,"504000":0,"513057":3,"514":0,"518":4,"521":2,"52726216":0,"52844271":0,"52877318":0,"53079236":0,"53193536":0,"533000":[0,2],"535000":0,"53602925":0,"53628662":0,"53628746":0,"53652122":0,"53746972":0,"539000":4,"540":2,"54040021":0,"542":0,"563000":0,"566000":2,"566078":3,"587":2,"587000":4,"59415564":0,"59z":[0,1,2,3,4],"600":[0,1,2,3,4],"614":[0,2],"623":3,"631095e":3,"635":2,"636630e":3,"637":4,"637278e":3,"637605e":3,"637658e":3,"637676e":3,"638075e":3,"638127e":3,"638134e":3,"638144e":3,"665418":0,"680":2,"682000":4,"701":0,"702":3,"7075568":3,"723":3,"726":4,"728":2,"771":2,"777":2,"793":4,"7abd7":[0,2],"800":1,"826000":4,"836000":4,"840580":0,"847":4,"857000":4,"85939727":0,"861000":4,"876":3,"880":0,"893051":3,"902394":0,"9027158":[3,4],"9070627":0,"91283397":2,"92359542":2,"92388773":2,"92394639":2,"92418747":2,"932000":4,"944":4,"953":3,"96304":3,"9673989":[3,4],"969000":4,"default":0,"float":3,"hodos\u00e1n":2,"import":[0,1,2,3,4],"int":[0,2,3,4],"new":4,"return":1,"true":[0,2,4],PIs:4,The:2,Yes:[0,2],_field2:2,_field:[0,1,2,3,4],_measur:[0,1,2,3,4],_result:[3,4],_start:[0,2,3,4],_stop:[0,2,3,4],_time:[0,2,3,4],_valu:[0,1,2,3,4],aav:4,acceler:[0,2],across:2,adam:0,adb:0,add:2,add_trac:[0,2],adriaan:[0,2],aensor:0,age:3,agg:[0,2,3,4],aggregatewindow:2,aioattribut:0,align:[1,2,3],all:[0,2,4],anchor:2,apach:[1,3],append:[0,2,4],appli:[1,2],approv:1,artefact:[3,4],ascend:[0,1,2,3,4],ashdown:2,astyp:[0,1,2,4],at2:0,at5:0,author:0,authornam:[0,2],avg:0,axi:[1,2],background:1,badg:4,bar:[0,2,4],barmod:[0,1,2,4],base:[3,4],beam:0,beer:[0,2],begin:2,bifrost:0,bin:3,black:[1,2,3],blue:3,bold:[1,2,3],book:5,branch:2,branch_id:0,branch_jira:[0,2],branchvalu:[1,3],brs:0,brs_jira:0,bsd:[1,3],bucket:[0,1,2,3,4],build:[0,2,3,4],calgrai:[0,2],campbel:0,cancel:3,caption:[1,2,3],carlo:2,cbf:0,center:[1,2,3],cgrai:[0,2],cgray_eigen_extens:0,cgray_ms_check:0,cicd:[0,3,4],claus:[1,3],client:[0,1,2,3,4],close:0,closest:2,code:[0,1,2,3,4],color:[1,2,3],color_discrete_map:[1,3],color_discrete_sequ:3,column:[0,1,2,3,4],com:1,common:[0,1,2,3,4],compar:2,complet:2,compon:4,config:[0,1,2,3,4],contain:5,copi:[0,2,4],core:2,count:[0,1,2,3,4],coverag:4,cpp:[3,4],creat:2,csp:[0,2,3,4],cumulativesum:2,current:[0,2,4],current_pi:[2,3,4],current_start:[0,2,4],current_timestamp:3,cursor:2,cut:3,dai:[2,3],daq:4,data:[0,2,4],date:2,datetim:[0,2,3],ddc:0,def:1,deffc74:2,del:4,deploy:2,desc:[0,2],develop:0,devereux:[0,2],devic:2,df_licens:1,df_licenses_tab:1,df_merge_requests_by_pi:2,df_mr_without_jira_by_pi:2,df_pipelin:3,df_pipeline_statu:3,df_project:4,df_state_ag:3,df_topn:2,dict:[0,1,2,3,4],discret:1,distinct:2,docker:0,dockerfil:0,dockeris:0,doing:4,drew:[0,2],drewdevereux:[0,2],drop:[0,1,2,3,4],dur:[0,2],durat:[0,2],each:2,els:1,entri:[1,3],env:0,everi:2,exampl:[3,4],express:[1,2,3],extern:0,fail:3,fals:[0,1,2,3],ffb54c:[0,2],fff:[1,3],fig:[0,1,2,3,4],figur:[0,2,4],filter:[0,1,2,3,4],finish:[0,1,2,3,4],first_br_trac:0,first_mr_trac:[0,2],fix:0,follow:2,font:[1,2,3],format:[0,2,3,4],from:[0,1,2,3,4],func:0,gabriella:2,gener:4,gerhard:2,gianluca:[0,2],gianlucamarotta:[0,2],gitlab:4,gitlab_metr:[0,1,2,3,4],gitop:4,give:2,goldenrod:3,got_coverag:[1,3,4],got_lint:[1,3,4],got_rtd:[1,3,4],got_test:[1,3,4],gpl:1,graph_object:[0,2,4],green:3,gridcolor:[0,2,4],gridwidth:[0,2,4],grm84:[0,2],group:[0,1,2,3,4],groupbi:[0,2,3,4],hard:[0,2],have:2,health:3,height:[0,1,2,3,4],hide_index:[1,2,3],highlight_max:1,highlight_row:1,horizont:[1,3],hover:2,hoverinfo:[1,3],hoverlabel_align:2,hovermod:2,how:[2,4],how_mani:3,http:1,icrar:[0,2],idg:0,imag:[0,2,3,4],includ:[0,1,2,3,4],increment:[0,2,4,5],index:0,inf:3,inplac:[0,2,4],insidetextorient:[1,3],integr:4,ipynb:[0,1,2,3,4],isin:1,isstal:0,jira:[0,2],join:[0,2,4],k8s:0,kalyanithigal:2,kei:[0,2,4],label:[1,2,3],lambda:2,last:[2,3],last_coverag:1,last_pipeline_dur:4,last_start:[0,2,4],leaf:[1,3],leap:[0,2],left:[1,2,3],legend:[2,3],len:[1,3],lgpl:1,licens:[3,4],life:2,lightgrei:[0,2,4],line:[1,2,3],lint:4,lipscomb:2,lmc:[0,2,3,4],loc:[0,1,2,3],low:[0,2,4],luminosa42:0,mani:2,margin:[1,3],mark:2,marker:[1,3],marker_color:[0,2],marotta:[0,2],matteo:2,maxiv:0,mcc:[0,2],mcs:0,merg:[0,4],merge_request:[0,2],merged_bi:[0,2],metric:4,mid:0,mit:1,mmarquar:2,mode:2,model:[3,4],mr_id:[0,2],mrs:[0,2,4],mrs_branch_jira:0,mrs_jira:2,must:2,name:[0,1,2,3,4],never:3,nijin:2,nijinjose123:2,no_jira:[0,2],normal:2,now:3,nuniqu:[0,2,3,4],oauth2:2,ofbranch:0,offlin:2,old:3,old_threshold:3,opac:[1,3],opflask:4,order:[0,2,4],org:0,oskar:[3,4],oso:4,other:[1,3],output:[0,2,4],outsid:[0,2,4],over:4,overlai:2,packag:0,panda:3,paper_bgcolor:[0,1,2,3,4],par:[3,4],path:[1,3],pdm:4,percent:[0,1,2,3],phase:2,pi10:[0,1,2,3,4],pi6:[0,2,4],pi7:[0,2,4],pi8:[0,2,4],pi9:[0,2,4],pi_list:[0,2,4],pie:[1,3],pier:[0,2],piershard:[0,2],pink:1,pio:2,pip:0,pipelin:[2,4],pipeline_last_run_finish:3,pipeline_last_run_statu:[1,3,4],piqm_bootstrap:[0,1,2,3,4],pis:[0,2,4],plainnam:1,plot:2,plot_bgcolor:[0,1,2,3,4],plotli:[0,1,2,3,4],pop:[0,2,4],print:[0,2,3,4],program:[0,2,4],programm:5,project:[0,1,2,3],project_branch:0,project_id:[0,2],project_stat:4,prop:[1,2,3],proxi:[0,2],psi:4,pss:4,publish:0,pull:[1,3],pyfabil:[0,4],python:[0,1],queri:[0,2,4],query_api:[0,1,2,3,4],query_data_fram:[0,1,2,3,4],rang:[0,1,2,3,4],rascil:0,rdbu:3,rdma:0,readthedoc:4,receiv:0,recent:3,red:3,ref:[0,2],refer:2,reflect:4,remov:0,renam:[0,1,2,3,4],renaming_new:0,reposit:2,repositori:4,repres:2,represent:2,request:[0,4],reset_index:[0,2,3,4],respect:4,result:[0,2,3,4],revers:[0,2,4],revert:2,rgb:0,rgba:[0,1,2,3,4],right:3,rl_itdev:2,rodrigo:2,ross:2,roux:2,row:[0,2,3,4],rtd:4,rtobar:0,run:[0,1,2,3,4],safe:2,scienc:2,sdc:[0,3,4],sdi:[0,2,3,4],sdp:[0,2,3,4],second:2,select_pi:[0,2,4],selected_pi:[0,1,2,3,4],selector:[1,2,3],sep_pipeline_imag:0,sequenti:3,seri:[0,2,4],servic:0,set:[0,2,4],set_capt:[1,2,3],set_properti:[1,2,3],set_table_styl:[1,2,3],show:[0,1,2,3,4],showgrid:[0,2,4],showlegend:1,showlin:2,showspik:2,side:2,sim:[2,3,4],size:[0,1,2,3,4],ska:[0,1,2,3,4],skampi:0,skatelescop:0,skeleton:0,solid:2,sorgenskamm:0,sort:[0,2],sort_valu:[0,1,2,3,4],sourc:2,spikedash:2,spikemod:2,spikesnap:2,stack:[0,1,2],stack_label:[0,2,4],stale:[0,2,3],stale_pipelin:3,stale_threshold:3,standard:4,start:[0,1,2,3,4],state:[0,2],statu:[3,4],stewart:2,stfc:2,stop:[0,1,2,3,4],stori:2,story_at1:2,str:[1,3],stvguest:4,style:[1,2,3],subel:0,submiss:0,subset:[1,2,3],success:[2,3],sunburst:[1,3],tabl:[0,2,3,4],tango:[3,4],tangogql:0,telescop:[0,2,3,4],templat:[0,3,4],template_br_trac:0,template_mr_trac:[0,2],template_mrs_without_jira:2,template_project_trac:4,test:[0,4],text:[0,1,2,3,4],textfont_s:[1,3],textinfo:[1,3],textposit:[0,2,4],the_pi:[0,2,4],thi:5,thykkathu:2,ticket:2,tickfont_s:[0,1,2,3,4],tickformat:2,time1:[0,2],time2:[0,2],timeshift:[0,2],timestamp:3,titl:[0,1,2,3,4],title_x:[0,1,2,3,4],titlefont_s:[0,1,2,3,4],tmc:[2,3,4],toaxi:2,tobar:2,top:[2,3],total:[0,1,2,3,4],traceord:2,transport:0,trend:2,ugur:2,uint:[0,2],union:[0,2,4],uniqu:[0,2],unit:2,unknown:[0,1,3],updat:[0,2],update_layout:[0,1,2,3,4],update_trac:[0,1,2,3,4],update_xax:[0,2],update_yax:[0,2,4],usag:1,valid:[2,3,4],valu:[0,1,2,3,4],vector:4,version:0,web:0,weight:[1,2,3],when:2,which:2,white:[1,3],width:[1,3],william:2,without:2,work:2,wstep:0,xanchor:[2,3],xaxi:[0,2,4],xaxis2:2,yaml:[0,2],yan:[0,2],yanchor:[2,3],yaxi:[0,1,2,3,4],yaxis_domain:2,yes:[0,2],yield:[0,2,4],yilmaz:2,zip:[0,2]},titles:["Branches","Licenses","Merge Requests","Pipelines","Projects","PI10 Developer Practices Quality Metrics"],titleterms:{activ:2,author:2,branch:0,develop:5,highest:2,licens:1,merg:2,metric:5,over:2,pi10:5,pipelin:3,practic:5,project:4,qualiti:5,request:2,timefram:2}})