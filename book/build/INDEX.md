
The following are a series of Jupyter Books containing the GitLab Programme Increment Developer Metrics for each PI:

* [PI10 (start Wednesday, 2021-03-17, end Tuesday, 2021-06-15)](./pi10/)
* [PI11 (start Wednesday, 2021-06-16, end Wednesday, 2021-09-15)](./pi11/)
* [PI12 (start Thursday, 2021-09-16, end Thursday, 2021-12-16)](./pi12/)
* [PI13 (start Friday, 2021-12-17, end Friday, 2022-03-18)](./pi13/)
* [PI14 (start Saturday, 2022-03-19, end Saturday, 2022-06-18)](./pi14/)
* [PI15 (start Sunday, 2022-06-19, end Sunday, 2022-09-18)](./pi15/)
