# Write out Influx queries for a given PI


# Date p0 = Date.parse( 'yyyyMMdd', '20181212' ) // start of PI 1
# Integer incr = (p1-p0)/(13*7)+1 as Integer // calculate the current incr...each incr is 13 weeks of 7 days
# Integer sprint=(p1-p0)%(13*7)/14+1 as Integer // calculate the current sprint...find the mod of the incr...and divid by length of each sprint i.e. 14 days
# if (sprint>6) {
#     sprint=6
# }
# float piSprint = Integer.valueOf(incr)+Integer.valueOf(sprint)/10


current_pi = 9
first_pi = 1
select_pis = 5 + 1

first_commit_trace = """
from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "project_commits")
  |> filter(fn: (r) => r.default == "1")
  |> drop(columns:["commit_id", "jira", "jira_on_ref", "merged", "pi", "project", "project_id", "bare", "default", "ref", "who", "branch_name", "_measurement", "_field"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> set(key: "_field",value: "{0[pi]} ({0[started]})")
  |> yield(name: "{0[pi]}")
"""

template_commit_trace = """
time1{0[pi]} = uint(v: {0[start]})
time2{0[pi]} = uint(v: {0[current_start]})
dur{0[pi]} = duration(v: time2{0[pi]} - time1{0[pi]})

from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "project_commits")
  |> filter(fn: (r) => r.default == "1")
  |> drop(columns:["commit_id", "jira", "jira_on_ref", "merged", "pi", "project", "project_id", "bare", "default", "ref", "who", "branch_name", "_measurement", "_field"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> set(key: "_field",value: "{0[pi]} ({0[started]})")
  |> timeShift(duration: dur{0[pi]}, columns: ["_start", "_stop", "_time"])
  |> yield(name: "{0[pi]}")
"""

from datetime import datetime, timezone, timedelta

this_pi = datetime.strptime("2018-12-12", "%Y-%m-%d").replace(tzinfo=timezone.utc)
the_pis = []
last_start = ""
for pi in range(first_pi, current_pi + 1):
    end_date = this_pi + timedelta(days=(13 * 7) + 1)
    the_pis.append(
        {
            "pi": "PI{}".format(pi),
            "start": this_pi.strftime("%Y-%m-%dT%H:%M:%SZ"),
            "started": this_pi.strftime("%Y-%m-%d"),
            "finish": (end_date - timedelta(seconds=1)).strftime("%Y-%m-%dT%H:%M:%SZ"),
        }
    )
    last_start = this_pi
    this_pi = end_date

last_start = last_start.strftime("%Y-%m-%dT%H:%M:%SZ")

# output all the series in reverse order
pis = the_pis.copy()
current = pis.pop(-1)

#### Commits over time
print(first_commit_trace.format(current))
for pi in pis[: (current_pi - select_pis) : -1]:
    pi["current_start"] = last_start
    print(template_commit_trace.format(pi))


pi_list = ([current] + pis[: (current_pi - select_pis) : -1])[::-1]

print("--- Jira Commits")

template_jira_commits = """
t1{0[pi]} = from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "project_commits")
  |> filter(fn: (r) => r.default == "1")
  |> filter(fn: (r) => r.jira == "1")
  |> drop(columns:["commit_id", "jira", "jira_on_ref", "merged", "pi", "project", "project_id", "bare", "ref", "who", "branch_name", "_measurement", "_field"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> last()

t2{0[pi]} = from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "project_commits")
  |> filter(fn: (r) => r.default == "1")
  |> drop(columns:["commit_id", "jira", "jira_on_ref", "merged", "pi", "project", "project_id", "bare", "ref", "who", "branch_name", "_measurement", "_field"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> last()

{0[pi]} = join(tables: {{t1: t1{0[pi]}, t2: t2{0[pi]} }}, on: ["default"])
  |> map(fn: (r) => ({{ r with _value: (r._value_t1 * 100) / r._value_t2 }}))
  |> set(key: "_field",value: "{0[pi]}")
  |> drop(columns:["default"])
"""

for pi in pi_list:
    pi["current_start"] = last_start
    print(template_jira_commits.format(pi))

tables = ", ".join([pi["pi"] for pi in pi_list])
print(
    """
union(tables: [{}])
  |> yield(name: "jira")
""".format(
        tables
    )
)


print("--- Lines commit on default")

template_lines = """
{0[pi]} = from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "project_commits")
  |> filter(fn: (r) => r.default == "1")
  |> drop(columns:["commit_id", "project", "project_id", "default", "jira", "jira_on_ref", "merged", "pi", "bare", "ref", "who", "branch_name","_measurement", "_field"])
  |> sum()
  |> set(key: "_field",value: "{0[pi]}")
"""

for pi in pi_list:
    pi["current_start"] = last_start
    print(template_lines.format(pi))

tables = ", ".join([pi["pi"] for pi in pi_list])
print(
    """
union(tables: [{}])
  |> yield(name: "lines")
""".format(
        tables
    )
)


print("--- Bare commits on default")
template_bare_commits = """
{0[pi]} = from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "project_commits")
  |> filter(fn: (r) => r.default == "1")
  |> filter(fn: (r) => r.bare == "1")
  |> drop(columns:["commit_id", "jira", "jira_on_ref", "merged", "pi", "project", "project_id", "bare", "default", "ref", "who", "branch_name", "_measurement", "_field"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> last()
  |> set(key: "_field",value: "{0[pi]}")
"""

for pi in pi_list:
    pi["current_start"] = last_start
    print(template_bare_commits.format(pi))

tables = ", ".join([pi["pi"] for pi in pi_list])
print(
    """
union(tables: [{}])
  |> yield(name: "lines")
""".format(
        tables
    )
)

print("--- Branches without Jira")
template_branches_without_jira = """
{0[pi]} = from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "project_branches")
  |> filter(fn: (r) => r.isstale == "0")
  |> filter(fn: (r) => r.default == "0")
  |> filter(fn: (r) => r.jira == "0")
  |> drop(columns:["branch_id", "jira", "mr", "_field", "isstale", "merged", "default", "ref", "project_id", "project", "branch", "_measurement"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> last()
  |> set(key: "_field",value: "{0[pi]}")
"""

for pi in pi_list:
    pi["current_start"] = last_start
    print(template_branches_without_jira.format(pi))

tables = ", ".join([pi["pi"] for pi in pi_list])
print(
    """
union(tables: [{}])
  |> yield(name: "lines")
""".format(
        tables
    )
)


#### Merge Requests over time
print("--- Merge Requests over time")


first_mr_trace = """
from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "merge_requests")
  |> filter(fn: (r) => r.state == "merged")
  |> drop(columns:["mr_id", "state", "author", "jira", "project", "project_id", "merged_by", "ref", "stale", "_measurement", "_field"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> set(key: "_field",value: "{0[pi]} ({0[started]})")
  |> yield(name: "{0[pi]}")
"""

template_mr_trace = """
time1{0[pi]} = uint(v: {0[start]})
time2{0[pi]} = uint(v: {0[current_start]})
dur{0[pi]} = duration(v: time2{0[pi]} - time1{0[pi]})

from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "merge_requests")
  |> filter(fn: (r) => r.state == "merged")
  |> drop(columns:["mr_id", "state", "author", "jira", "project", "project_id", "merged_by", "ref", "stale", "_measurement", "_field"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> set(key: "_field",value: "{0[pi]} ({0[started]})")
  |> timeShift(duration: dur{0[pi]}, columns: ["_start", "_stop", "_time"])
  |> yield(name: "{0[pi]}")
"""


# output all the series in reverse order
pis = the_pis.copy()
current = pis.pop(-1)
print(first_mr_trace.format(current))
for pi in pis[: (current_pi - select_pis) : -1]:
    pi["current_start"] = last_start
    print(template_mr_trace.format(pi))


print("--- Merge Requests without Jira")
template_mrs_without_jira = """
{0[pi]} = from(bucket: "gitlab_metrics")
  |> range(start: {0[start]}, stop: {0[finish]})
  |> filter(fn: (r) => r._measurement == "merge_requests")
  |> filter(fn: (r) => r.state == "merged")
  |> filter(fn: (r) => r.jira == "0")
  |> drop(columns:["mr_id", "state", "author", "jira", "project", "project_id", "merged_by", "ref", "stale", "_measurement", "_field"])
  |> aggregateWindow(every: 1d, fn: count)
  |> cumulativeSum(columns: ["_value"])
  |> sort(columns: ["dateTime"], desc: false)
  |> last()
  |> set(key: "_field",value: "{0[pi]}")
"""

for pi in pi_list:
    pi["current_start"] = last_start
    print(template_mrs_without_jira.format(pi))

tables = ", ".join([pi["pi"] for pi in pi_list])
print(
    """
union(tables: [{}])
  |> yield(name: "lines")
""".format(
        tables
    )
)


# print("--- Merge Requests without Jira")
# template_mrs_without_jira = """
# {0[pi]}nj = from(bucket: "gitlab_metrics")
#   |> range(start: {0[start]}, stop: {0[finish]})
#   |> filter(fn: (r) => r._measurement == "merge_requests")
#   |> filter(fn: (r) => r.state == "merged")
#   |> filter(fn: (r) => r.jira == "0")
#   |> drop(columns:["mr_id", "state", "author", "jira", "project", "project_id", "merged_by", "ref", "stale", "_measurement", "_field"])
#   |> aggregateWindow(every: 1d, fn: count)
#   |> cumulativeSum(columns: ["_value"])
#   |> sort(columns: ["dateTime"], desc: false)
#   |> last()
#   |> set(key: "_field",value: "{0[pi]}")
#   |> rename(columns: {{_value: "hasjira"}})

# {0[pi]}hj = from(bucket: "gitlab_metrics")
#   |> range(start: {0[start]}, stop: {0[finish]})
#   |> filter(fn: (r) => r._measurement == "merge_requests")
#   |> filter(fn: (r) => r.state == "merged")
#   |> filter(fn: (r) => r.jira == "1")
#   |> drop(columns:["mr_id", "state", "author", "jira", "project", "project_id", "merged_by", "ref", "stale", "_measurement", "_field"])
#   |> aggregateWindow(every: 1d, fn: count)
#   |> cumulativeSum(columns: ["_value"])
#   |> sort(columns: ["dateTime"], desc: false)
#   |> last()
#   |> set(key: "_field",value: "{0[pi]}")
#   |> rename(columns: {{_value: "hasjira"}})

# {0[pi]} = join(
#   tables: {{ {0[pi]}nj, {0[pi]}hj }},
#   on: ["_field"]
# )
# """

# for pi in pi_list:
#     pi["current_start"] = last_start
#     print(template_mrs_without_jira.format(pi))

# tables = ", ".join([pi["pi"] for pi in pi_list])
# print(
#     """
# union(tables: [{}])
#   |> yield(name: "lines")
# """.format(
#         tables
#     )
# )


# SS-46 SS-47
# heat map on commits against projects

# reload project_commits
# branch count needs to be on refs from commits