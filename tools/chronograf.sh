#!/bin/sh

docker rm -f chronograf

docker run -p 8888:8888 --name=chronograf --net=host -d chronograf:1.8.6 --influxdb-url=http://localhost:8086

