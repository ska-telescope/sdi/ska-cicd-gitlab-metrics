import pickle
import pandas as pd
import click
import yaml
import os
import sys
import re
from datetime import datetime, timezone, timedelta
import asyncio
from aioinflux import InfluxDBClient
import logging
from voluptuous import (
    ALLOW_EXTRA,
    All,
    Invalid,
    Required,
    Schema,
    Url,
    IsDir,
    MultipleInvalid,
)

global_config_schema = Schema(
    {
        Required("db", default="gitlab_metrics"): str,
        Required("db_host", default="localhost"): str,
        Required("db_port", default=8086): int,
        Required(
            "state_dir", default="./state"
        ): IsDir(),  # pylint: disable=no-value-for-parameter
        Required("commit_age", default=90): int,  # 90 days or 3 months or 1 PI
        Required(
            "stale_merge_requests", default=30
        ): int,  # 30 days or 4 weeks or 2 Sprints
        Required("exclude_metrics", default=[]): list([str]),
        Required("include_metrics", default=[]): list([str]),
    },
    extra=True,
)
config_schema = Schema(
    All(
        {
            Required("global", default={}): global_config_schema,
        },
    )
)


COMMITS_MEASUREMENT = "project_commits"

TAGS = [
    "project_id",
    "project",
    "commit_id",
    "branch_name",
    "ref",
    "bare",
    "jira",
    "jira_on_ref",
    "who",
    "default",
    "merged",
    "pi",
]

# calculate the date breaks for the PIs
this_pi = datetime.strptime("2018-12-12", "%Y-%m-%d").replace(tzinfo=timezone.utc)
PIS = []
for pi in range(1, 21):
    end_date = this_pi + timedelta(days=(13 * 7) + 1)
    PIS.append({"PI": "PI{}".format(pi), "start": this_pi, "finish": end_date})
    this_pi = end_date

log = logging.getLogger("load_commit_state")

logging_level = os.environ.get("LOGGING_LEVEL", "ERROR")
logging_format = (
    # "%(asctime)s [level=%(levelname)s] [thread=%(threadName)s] "
    # "[module=%(module)s] [line=%(lineno)d]: %(message)s"
    "%(asctime)s [level=%(levelname)s] "
    "[line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=logging_level, format=logging_format)

jira_search = r".*[a-zA-Z][a-zA-Z0-9]+\-[0-9]+.*"
jira_regex = re.compile(jira_search)

# check all state files can be loaded
commit_state_file = "./state/commits.pickle"
with open(commit_state_file, "rb") as f:
    commit_state = pickle.load(f)

branch_state_file = "./state/branches.pickle"
with open(branch_state_file, "rb") as f:
    branch_state = pickle.load(f)

group_state_file = "./state/groups.pickle"
with open(group_state_file, "rb") as f:
    group_state = pickle.load(f)

project_state_file = "./state/projects.pickle"
with open(project_state_file, "rb") as f:
    project_state = pickle.load(f)

rtd_state_file = "./state/rtds.pickle"
with open(rtd_state_file, "rb") as f:
    rtd_state = pickle.load(f)

badge_state_file = "./state/badges.pickle"
with open(badge_state_file, "rb") as f:
    badge_state = pickle.load(f)

mr_state_file = "./state/mrs.pickle"
with open(mr_state_file, "rb") as f:
    mr_state = pickle.load(f)


def which_pi(commit_date):
    # time of commit parsed to a PI label
    try:
        commit_date = datetime.strptime(commit_date, "%Y-%m-%dT%H:%M:%S.%f%z")
    except ValueError:
        from dateutil import parser

        commit_date = parser.parse(commit_date)
    for pi in PIS:
        if commit_date < pi["finish"] and commit_date >= pi["start"]:
            return pi["PI"]
    return "Unknown"


async def process_commits(client, real=False, default_flag=False):
    """Process Commits"""

    # process each project
    for project_id in commit_state.keys():

        # get the project cache
        project = project_state[project_id]
        log.debug("project: " + repr(project))
        log.debug("branches in project: " + repr(branch_state[project_id].keys()))

        # find the default branch name
        default_branch = [
            branch for branch in branch_state[project_id].values() if branch["default"]
        ][0]["name"]
        log.debug("Default branch: {}".format(default_branch))

        # find all the commits on the default branch
        commit_ids = [
            {
                "commit_id": commit["id"],
                "branch": branch_name,
                "time": commit["authored_date"],
            }
            for branch_name in branch_state[project_id].keys()
            if branch_name in commit_state[project_id]
            for commit in commit_state[project_id][branch_name]["commits"].values()
        ]
        commit_ids = pd.DataFrame(commit_ids)
        commit_ids["time"] = pd.to_datetime(commit_ids.time, utc=True)
        commit_ids.sort_values(by=["time"], inplace=True)
        commit_ids.index = pd.DatetimeIndex(commit_ids.time)
        log.debug("commit_ids: {}".format(commit_ids))

        # commit branches are deleted mostly on merging so wont march branch_state[project_id]
        #   but may just get deleted by the author
        log.debug("commit branches(refs): {}".format(commit_state[project_id].keys()))

        # process each branch in the project
        # this means that commits are actually repeated as they appear in more than one branch
        # XXX maybe we want to only have refs and the default ?
        for branch in commit_state[project_id].keys():

            # skip commits on other branches than the default
            if default_flag and not branch == default_branch:
                continue

            log.info(
                "Processing branch: {}[{}]/{}".format(
                    project["path_with_namespace"], project_id, branch
                )
            )

            points = []
            for commit in commit_state[project_id][branch]["commits"].values():
                bare = 0
                default = 0
                merged = 0

                # the merge state can change over time as merge may
                # be in the future
                commit_on_default = len(
                    commit_ids[
                        (commit_ids["commit_id"] == commit["id"])
                        & (commit_ids["branch"] == default_branch)
                    ].index
                )
                if commit_on_default > 0 and not commit["ref"] == default_branch:
                    merged = 1

                # determine if it is on the default branch directly
                if branch == default_branch:
                    default = 1

                # is this a bare commit ?
                if commit["ref"] == default_branch:
                    bare = 1

                log.debug(
                    "Commit: {} ref: {} PI: {} bare: {} default: {} merged: {}".format(
                        commit["id"],
                        commit["ref"],
                        which_pi(commit["authored_date"]),
                        bare,
                        default,
                        merged,
                    )
                )

                points.append(
                    {
                        "time": commit["authored_date"],
                        "project_id": str(project_id),
                        "project": project["path_with_namespace"],
                        "commit_id": str(commit["id"]),
                        "branch_name": branch,
                        "ref": commit["ref"],
                        "merged": str(merged),
                        "pi": which_pi(commit["authored_date"]),
                        "bare": str(bare),
                        "jira": str(0 if not jira_regex.match(commit["title"]) else 1),
                        "jira_on_ref": str(
                            0 if not jira_regex.match(commit["ref"]) else 1
                        ),
                        "who": commit["author_name"]
                        + " ["
                        + commit["author_email"]
                        + "]",
                        "default": str(default),
                        "total": float(commit["stats"]["total"]),
                    }
                )

            points = [
                {
                    k: (
                        p[k]
                        .replace("\\", "")
                        .replace(" ", "\ ")
                        .replace("=", "")
                        .replace(",", "")
                        if k in TAGS
                        else p[k]
                    )
                    for k in p.keys()
                }
                for p in points
            ]

            points = pd.DataFrame(points)
            points["time"] = pd.to_datetime(points.time, utc=True)
            points.sort_values(by=["time"], inplace=True)
            points.index = pd.DatetimeIndex(points.time)

            points = points.drop(["time"], axis=1)

            log.debug(points.dtypes)
            log.debug("Commits per branch: {} - {}".format(branch, len(points.index)))

            dupes = points.groupby(["branch_name", "commit_id"]).size()

            if len(dupes[dupes > 1]) > 1:
                log.info("branch/commit_id: {} ".format(dupes[dupes > 1]))

            # Do we really want to do the update ?
            if real:
                await client.write(
                    points, measurement=COMMITS_MEASUREMENT, tag_columns=TAGS
                )
            log.info(
                "Processed branch: {}[{}]/{} = {}".format(
                    project["path_with_namespace"],
                    project_id,
                    branch,
                    len(points.index),
                )
            )


async def main_loop(config, real, default_flag):
    """Process pickled gitlab harvested data into InfluxDB"""
    async with InfluxDBClient(
        host=config["db_host"], port=config["db_port"], db=config["db"], timeout=1800
    ) as client:
        await process_commits(client, real, default_flag)


def validate_config_file(ctx, param, value):
    try:
        return config_schema(yaml.load(value, Loader=yaml.FullLoader))
    except yaml.YAMLError as e:
        raise click.BadParameter(str(e), ctx=ctx, param=param)
    except MultipleInvalid as e:
        raise click.BadParameter(e.msg, ctx=ctx, param=param, param_hint=e.path)


@click.command()
@click.argument(
    "config",
    envvar="CONFIG_PATH",
    callback=validate_config_file,
    type=click.File("r"),
    default="./gitlab-metrics.yml",
)
@click.option("--real/--not-real", default=False)
@click.option("--default/--not-default", default=False)
def main(config: dict = None, real: bool = False, default: bool = False):

    config = config["global"]
    log.debug("config: {}".format(config))

    # Kick off the main loop
    asyncio.get_event_loop().run_until_complete(main_loop(config, real, default))


if __name__ == "__main__":
    main()
