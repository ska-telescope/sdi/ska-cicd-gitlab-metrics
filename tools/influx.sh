#!/bin/sh

docker run --rm -ti --net=host influxdb:1.8.5 bash -c "influx -type flux -host 192.168.99.37 -port 8086 -path-prefix /api/v2/query"
