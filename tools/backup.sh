#!/bin/sh

set -e
set -x

INFLUXDB_NAME=influxdb
DATA_DIR=/data/influxdb
STATE_DIR=./state

DTE=`date +"%Y.%m.%d.%H%M%S"`
BACKUP=./backup-${DTE}

docker stop ${INFLUXDB_NAME} || true

mkdir -p ${BACKUP}
rsync -av  ${STATE_DIR} ${BACKUP}/
sudo rsync -a ${DATA_DIR} ${BACKUP}/
sudo chown -R ${USER}: ${BACKUP}
du -s -m ${BACKUP}

docker start ${INFLUXDB_NAME}

