# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

GITLAB_TOKEN ?= the-token
RTD_TOKEN ?= another-token
LOGGING_LEVEL ?= INFO
# FLAGS ?= --parseandexit
# LOAD_FLAGS ?= --selectedpi=22
LOAD_FLAGS ?=
GQL_LOAD_FLAGS ?= $(LOAD_FLAGS)

DOCKER_BUILD_ARGS ?= --build-arg base_image="python:3.11"
KUBE_NAMESPACE ?= metrics

HELM_RELEASE ?= gitlabmetrics
TEST_RUNNER ?= runner-$(CI_JOB_ID)-$(RELEASE_NAME)
BASE = $(shell pwd)

GAC ?= jira-reporting-poc-79f1ef7738d1.json

AFTER ?= 2023-12-31
BEFORE ?= $(shell date +%Y-%m-%d)

.DEFAULT_GOAL := help

-include .make/base.mk
-include .make/oci.mk
-include .make/python.mk

# define private rules and additional makefiles
-include PrivateRules.mak

TAG ?= $(shell . $(RELEASE_SUPPORT); getTag)
IMG ?= registry.gitlab.com/ska-telescope/sdi/ska-cicd-gitlab-metrics/ska-cicd-gitlab-metrics:$(TAG)


PYTHON_LINT_TARGET=$(PYTHON_SRC)
PYTHON_SWITCHES_FOR_FLAKE8=--ignore=E1,E23,W503,E501,E722
PYTHON_SWITCHES_FOR_PYLINT=--disable=C0301,C0103,C0209,C0116,C0114,W1202,W1201,R0913,R1716,W0702,C0302,W3101,C0201,R0801
PYTHON_VARS_BEFORE_PYTEST+=  PYTHONPATH=$(BASE)/src GITLAB_TOKEN=$(GITLAB_TOKEN) RTD_TOKEN=$(RTD_TOKEN)

PYTHON_VARS_AFTER_PYTEST=-rs -p no:molecule --cov=ska --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml
PYTHON_TEST_FILE=tests/unit/*.py

metrics-vars:
	@echo GITLAB_API_REQUESTER=$(GITLAB_API_REQUESTER)
	@echo GITLAB_API_PRIVATE_TOKEN=$(GITLAB_API_PRIVATE_TOKEN)
	@echo RTD_TOKEN=$(RTD_TOKEN)
	@echo IMG=$(IMG)

metrics-build:
	docker image rm $(IMG)
	make oci-build
	docker tag artefact.skao.int/ska-cicd-gitlab-metrics:$(TAG) $(IMG)
	docker push $(IMG)

metrics-shell:  ## Run poetry shell
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	GITLAB_TOKEN=$(GITLAB_TOKEN) RTD_TOKEN=$(RTD_TOKEN) \
	LOGGING_LEVEL=$(LOGGING_LEVEL) poetry shell

metrics-get-licenses:  ## extract licenses
	PYTHONPATH=./src GITLAB_TOKEN=$(GITLAB_TOKEN) RTD_TOKEN=$(RTD_TOKEN) \
	GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	LOGGING_LEVEL=$(LOGGING_LEVEL) poetry run python get_project_licenses.py $(GQL_LOAD_FLAGS)

metrics-graphql: ## Run GraphQL query
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	 RTD_TOKEN=$(RTD_TOKEN) \
	 GITLAB_TOKEN=$(GITLAB_TOKEN) poetry run python3 src/load-graphql-to-bigquery.py \
	--datasets "datasets" \
	 --after "$(AFTER)" --before "$(BEFORE)" $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-graphql-projects: ## Run GraphQL query
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	 RTD_TOKEN=$(RTD_TOKEN) \
	 GITLAB_TOKEN=$(GITLAB_TOKEN) poetry run python3 src/load-graphql-to-bigquery.py  \
	--datasets "datasets" \
	--skipgroups --skipmrs --skippipelines --after "$(AFTER)" --before "$(BEFORE)" $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-graphql-mrs: ## Run GraphQL query
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	 RTD_TOKEN=$(RTD_TOKEN) \
	 GITLAB_TOKEN=$(GITLAB_TOKEN) poetry run python3 src/load-graphql-to-bigquery.py  \
	--datasets "datasets" \
	--skipgroups --skipprojects --skippipelines --after "$(AFTER)" --before "$(BEFORE)" $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-graphql-pipelines: ## Run GraphQL query
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	 RTD_TOKEN=$(RTD_TOKEN) \
	 GITLAB_TOKEN=$(GITLAB_TOKEN) poetry run python3 src/load-graphql-to-bigquery.py  \
	--datasets "datasets" \
	--skipgroups --skipprojects --skipmrs --after "$(AFTER)" --before "$(BEFORE)" $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-graphql-dump-projects: ## Run GraphQL query and dump JSON
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	 RTD_TOKEN=$(RTD_TOKEN) \
	 GITLAB_TOKEN=$(GITLAB_TOKEN) poetry run python3 src/load-graphql-to-bigquery.py \
	--datasets "datasets" \
	--after "$(AFTER)" --before "$(BEFORE)" \
	--dump --skipgroups --skipmrs --skippipelines $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-graphql-dump: ## Run GraphQL query and dump JSON
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	 RTD_TOKEN=$(RTD_TOKEN) \
	 GITLAB_TOKEN=$(GITLAB_TOKEN) poetry run python3 src/load-graphql-to-bigquery.py \
	--datasets "datasets" \
	--after "$(AFTER)" --before "$(BEFORE)" \
	--skipprojects \
	--dump --skipupdate $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-graphql-load: ## load from JSON
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	 RTD_TOKEN=$(RTD_TOKEN) \
	 GITLAB_TOKEN=$(GITLAB_TOKEN) poetry run python3 src/load-graphql-to-bigquery.py --load \
	--skipgroups --skipprojects $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-docker-pull: ## pull image
	docker pull $(IMG)

metrics-docker-graphql-dump-projects: metrics-docker-pull ## Run GraphQL query and dump JSON
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	sudo rm -f $(BASE)/datasets/*.json
	mkdir -p $(BASE)/datasets && chmod a+rwx $(BASE)/datasets
	docker run --rm -t \
	-v $(BASE)/$(GAC):/etc/$(GAC) \
	-v $(BASE)/datasets:/datasets \
	-e GOOGLE_APPLICATION_CREDENTIALS=/etc/$(GAC) \
	-e  RTD_TOKEN=$(RTD_TOKEN) \
	-e GITLAB_TOKEN=$(GITLAB_TOKEN) \
	-e LOGGING_LEVEL=$(LOGGING_LEVEL) \
	$(IMG) --skipgroups --skipmrs --skippipelines \
	--datasets /datasets \
	--after "$(AFTER)" --before "$(BEFORE)" \
	--dump --skipupdate $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-docker-graphql-dump: metrics-docker-pull ## Run GraphQL query and dump JSON
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	sudo rm -f $(BASE)/datasets/*.json
	mkdir -p $(BASE)/datasets && chmod a+rwx $(BASE)/datasets
	docker run --rm -t --name gitlabmetricsdump \
	-v $(BASE)/$(GAC):/etc/$(GAC) \
	-v $(BASE)/datasets:/datasets \
	-e GOOGLE_APPLICATION_CREDENTIALS=/etc/$(GAC) \
	-e  RTD_TOKEN=$(RTD_TOKEN) \
	-e GITLAB_TOKEN=$(GITLAB_TOKEN) \
	-e LOGGING_LEVEL=$(LOGGING_LEVEL) \
	$(IMG) \
	--skipprojects \
	--datasets /datasets \
	--after "$(AFTER)" --before "$(BEFORE)" \
	--dump --skipupdate $(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-docker-graphql-load: metrics-docker-pull ## load from JSON
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	docker run --rm -t --name gitlabmetricsload \
	-v $(BASE)/$(GAC):/etc/$(GAC) \
	-v $(BASE)/datasets:/datasets \
	-e GOOGLE_APPLICATION_CREDENTIALS=/etc/$(GAC) \
	-e  RTD_TOKEN=$(RTD_TOKEN) \
	-e GITLAB_TOKEN=$(GITLAB_TOKEN) \
	-e LOGGING_LEVEL=$(LOGGING_LEVEL) \
	$(IMG) \
	--load \
	--datasets /datasets \
	$(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-docker-graphql-load-mrs: metrics-docker-pull ## load from JSON
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	docker run --rm -t --name gitlabmetricsload \
	-v $(BASE)/$(GAC):/etc/$(GAC) \
	-v $(BASE)/datasets:/datasets \
	-e GOOGLE_APPLICATION_CREDENTIALS=/etc/$(GAC) \
	-e  RTD_TOKEN=$(RTD_TOKEN) \
	-e GITLAB_TOKEN=$(GITLAB_TOKEN) \
	-e LOGGING_LEVEL=$(LOGGING_LEVEL) \
	$(IMG) \
	--load \
	--datasets /datasets \
	--skipgroups --skipprojects --skippipelines \
	$(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"

metrics-docker-graphql-load-pipelines: metrics-docker-pull ## load from JSON
	@echo "Start: $$(date) with GQL_LOAD_FLAGS=$(GQL_LOAD_FLAGS)"
	docker run --rm -t --name gitlabmetricsload \
	-v $(BASE)/$(GAC):/etc/$(GAC) \
	-v $(BASE)/datasets:/datasets \
	-e GOOGLE_APPLICATION_CREDENTIALS=/etc/$(GAC) \
	-e  RTD_TOKEN=$(RTD_TOKEN) \
	-e GITLAB_TOKEN=$(GITLAB_TOKEN) \
	-e LOGGING_LEVEL=$(LOGGING_LEVEL) \
	$(IMG) \
	--load \
	--datasets /datasets \
	--skipgroups --skipprojects --skipmrs \
	$(GQL_LOAD_FLAGS)
	@echo "Finish: $$(date)"



metrics-test-rtd: ## Run GraphQL query and dump JSON
	PYTHONPATH=./src GOOGLE_APPLICATION_CREDENTIALS=$(GAC) \
	 RTD_TOKEN=$(RTD_TOKEN) \
	 GITLAB_TOKEN=$(GITLAB_TOKEN) poetry run python3 test_rtd.py \

