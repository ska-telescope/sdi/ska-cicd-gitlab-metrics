ARG BASE_IMAGE="python:3.10"
# suppress - DL3006 warning: Always tag the version of an image explicitly
# hadolint ignore=DL3006
FROM ${BASE_IMAGE}
LABEL \
      author="Piers Harding <Piers.Harding@skao.int>" \
      description="This image harvests gitlab metrics" \
      license="Apache2.0" \
      org.skatelescope.team="Systems Team" \
      org.skatelescope.website="https://gitlab.com/ska-telescope/sdi/ska-cicd-gitlab-metrics"

# disable interactive mode for apt
ENV DEBIAN_FRONTEND noninteractive

# set base line logging level for metrics collector
ENV LOGGING_LEVEL=INFO

# Set working directory
WORKDIR /tmp

# Install Poetry
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN pip3 install poetry && \
    poetry config virtualenvs.create false

# Copy poetry.lock* in case it doesn't exist in the repo
COPY pyproject.toml poetry.lock* ./

# Install runtime dependencies
RUN poetry install --no-dev --no-root

# copy over application
COPY ./src/*.py /

# exec
RUN chmod a+x /*.py

# mount point for configuration
RUN mkdir /etc/gitlab-metrics
VOLUME ["/etc/gitlab-metrics"]
COPY ./gitlab-metrics.yml /etc/gitlab-metrics/gitlab-metrics.yml

# default launcher for app
# CMD ["--config", "/etc/gitlab-metrics/gitlab-metrics.yml"]
ENTRYPOINT ["/load-graphql-to-bigquery.py"]
