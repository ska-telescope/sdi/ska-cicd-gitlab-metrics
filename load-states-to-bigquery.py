#!/usr/bin/env python3

import logging
import os
import pickle
import re
import sys
from datetime import datetime, timezone, timedelta
import click
import pandas
from google.cloud import bigquery
import gitlab
import json

LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "ERROR").upper()
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] [thread=%(threadName)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("load-bigquery")

# find Jira issue number in text
jira_search = r"([a-zA-Z][a-zA-Z0-9]+\-[0-9]+).*"
jira_regex = re.compile(jira_search)



group_state_file = "./state/groups.pickle"
with open(group_state_file, "rb") as f:
    group_state = pickle.load(f)

branch_state_file = "./state/branches.pickle"
with open(branch_state_file, "rb") as f:
    branch_state = pickle.load(f)

project_state_file = "./state/projects.pickle"
with open(project_state_file, "rb") as f:
    project_state = pickle.load(f)

rtd_state_file = "./state/rtds.pickle"
with open(rtd_state_file, "rb") as f:
    rtd_state = pickle.load(f)

badge_state_file = "./state/badges.pickle"
with open(badge_state_file, "rb") as f:
    badge_state = pickle.load(f)

mr_state_file = "./state/mrs.pickle"
with open(mr_state_file, "rb") as f:
    mr_state = pickle.load(f)

mr_state_extended_file = "./state/mrs-extended.pickle"
if os.path.isfile(mr_state_extended_file):
    with open(mr_state_extended_file, "rb") as f:
        mr_extended_state = pickle.load(f)
else:
    mr_extended_state = {}

mr_state_commit_file = "./state/mrs-commit.pickle"
if os.path.isfile(mr_state_commit_file):
    with open(mr_state_commit_file, "rb") as f:
        mr_commit_state = pickle.load(f)
else:
    mr_commit_state = {}

pipeline_state_file = "./state/pipelines.pickle"
with open(pipeline_state_file, "rb") as f:
    pipeline_state = pickle.load(f)

PROJECT_KEYS = {
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "description": bigquery.enums.SqlTypeNames.STRING,
    "name": bigquery.enums.SqlTypeNames.STRING,
    "name_with_namespace": bigquery.enums.SqlTypeNames.STRING,
    "path": bigquery.enums.SqlTypeNames.STRING,
    "path_with_namespace": bigquery.enums.SqlTypeNames.STRING,
    "created_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "default_branch": bigquery.enums.SqlTypeNames.STRING,
    "ssh_url_to_repo": bigquery.enums.SqlTypeNames.STRING,
    "http_url_to_repo": bigquery.enums.SqlTypeNames.STRING,
    "web_url": bigquery.enums.SqlTypeNames.STRING,
    "readme_url": bigquery.enums.SqlTypeNames.STRING,
    "license_url": bigquery.enums.SqlTypeNames.STRING,
    "license": bigquery.enums.SqlTypeNames.STRING,
    "avatar_url": bigquery.enums.SqlTypeNames.STRING,
    # "forks_count": bigquery.enums.SqlTypeNames.INTEGER, # pandas.errors.IntCastingNaNError: Cannot convert non-finite values (NA or inf) to integer
    "star_count": bigquery.enums.SqlTypeNames.INTEGER,
    "last_activity_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "name": bigquery.enums.SqlTypeNames.STRING,
    "path": bigquery.enums.SqlTypeNames.STRING,
    "avatar_url": bigquery.enums.SqlTypeNames.STRING,
    "web_url": bigquery.enums.SqlTypeNames.STRING,
    "container_registry_image_prefix": bigquery.enums.SqlTypeNames.STRING,
    "empty_repo": bigquery.enums.SqlTypeNames.BOOLEAN,
    "archived": bigquery.enums.SqlTypeNames.BOOLEAN,
    "visibility": bigquery.enums.SqlTypeNames.BOOLEAN,
    "creator_id": bigquery.enums.SqlTypeNames.INTEGER,
    "time": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "project": bigquery.enums.SqlTypeNames.STRING,
    "pipeline_last_run_status": bigquery.enums.SqlTypeNames.STRING,
    "got_tests": bigquery.enums.SqlTypeNames.BOOLEAN,
    "got_linting": bigquery.enums.SqlTypeNames.BOOLEAN,
    "got_coverage": bigquery.enums.SqlTypeNames.BOOLEAN,
    "got_rtd": bigquery.enums.SqlTypeNames.BOOLEAN,
    "pipeline_time_since_last_run_seconds": bigquery.enums.SqlTypeNames.FLOAT,
    "pipeline_last_run_finished": bigquery.enums.SqlTypeNames.FLOAT,
    "last_coverage": bigquery.enums.SqlTypeNames.FLOAT,
    "last_pipeline_duration": bigquery.enums.SqlTypeNames.FLOAT,
    "scan_info": bigquery.enums.SqlTypeNames.FLOAT,
    "scan_unknown": bigquery.enums.SqlTypeNames.FLOAT,
    "scan_low": bigquery.enums.SqlTypeNames.FLOAT,
    "scan_medium": bigquery.enums.SqlTypeNames.FLOAT,
    "scan_high": bigquery.enums.SqlTypeNames.FLOAT,
    "scan_critical": bigquery.enums.SqlTypeNames.FLOAT,
    "scan_total": bigquery.enums.SqlTypeNames.FLOAT,
    "pi": bigquery.enums.SqlTypeNames.INTEGER,
    "sprint": bigquery.enums.SqlTypeNames.INTEGER,
}

BRANCH_KEYS = {
    "name": bigquery.enums.SqlTypeNames.STRING,
    "web_url": bigquery.enums.SqlTypeNames.STRING,
    "merged": bigquery.enums.SqlTypeNames.BOOLEAN,
    "protected": bigquery.enums.SqlTypeNames.BOOLEAN,
    "default": bigquery.enums.SqlTypeNames.BOOLEAN,
    "web_url": bigquery.enums.SqlTypeNames.STRING,
    "isstale": bigquery.enums.SqlTypeNames.BOOLEAN,
    "jira": bigquery.enums.SqlTypeNames.BOOLEAN,
    "jira_id": bigquery.enums.SqlTypeNames.STRING,
    "mr": bigquery.enums.SqlTypeNames.BOOLEAN,
    "time_since": bigquery.enums.SqlTypeNames.FLOAT,
    "time": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "branch_id": bigquery.enums.SqlTypeNames.STRING,
    "project": bigquery.enums.SqlTypeNames.STRING,
    "project_id": bigquery.enums.SqlTypeNames.STRING,
    "ref": bigquery.enums.SqlTypeNames.STRING,
    "age": bigquery.enums.SqlTypeNames.INTEGER,
    "pi": bigquery.enums.SqlTypeNames.INTEGER,
    "sprint": bigquery.enums.SqlTypeNames.INTEGER,
}


MR_KEYS = {
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "iid": bigquery.enums.SqlTypeNames.INTEGER,
    "project_id": bigquery.enums.SqlTypeNames.INTEGER,
    "title": bigquery.enums.SqlTypeNames.STRING,
    "description": bigquery.enums.SqlTypeNames.STRING,
    "created_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "updated_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "mu_username": bigquery.enums.SqlTypeNames.STRING,
    "mu_name": bigquery.enums.SqlTypeNames.STRING,
    "mu_state": bigquery.enums.SqlTypeNames.STRING,
    "mu_avatar_url": bigquery.enums.SqlTypeNames.STRING,
    "mu_web_url": bigquery.enums.SqlTypeNames.STRING,
    "merged_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "closed_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "target_branch": bigquery.enums.SqlTypeNames.STRING,
    "source_branch": bigquery.enums.SqlTypeNames.STRING,
    "user_notes_count": bigquery.enums.SqlTypeNames.INTEGER,
    "upvotes": bigquery.enums.SqlTypeNames.INTEGER,
    "downvotes": bigquery.enums.SqlTypeNames.INTEGER,
    "author": bigquery.enums.SqlTypeNames.STRING,
    # "assignee",
    "au_username": bigquery.enums.SqlTypeNames.STRING,
    "au_name": bigquery.enums.SqlTypeNames.STRING,
    "au_state": bigquery.enums.SqlTypeNames.STRING,
    "au_avatar_url": bigquery.enums.SqlTypeNames.STRING,
    "au_web_url": bigquery.enums.SqlTypeNames.STRING,
    "source_project_id": bigquery.enums.SqlTypeNames.STRING,
    "target_project_id": bigquery.enums.SqlTypeNames.STRING,
    "draft": bigquery.enums.SqlTypeNames.BOOLEAN,
    "work_in_progress": bigquery.enums.SqlTypeNames.BOOLEAN,
    "merge_when_pipeline_succeeds": bigquery.enums.SqlTypeNames.BOOLEAN,
    "merge_status": bigquery.enums.SqlTypeNames.STRING,
    "detailed_merge_status": bigquery.enums.SqlTypeNames.STRING,
    "sha": bigquery.enums.SqlTypeNames.STRING,
    "merge_commit_sha": bigquery.enums.SqlTypeNames.STRING,
    "squash_commit_sha": bigquery.enums.SqlTypeNames.STRING,
    "discussion_locked": bigquery.enums.SqlTypeNames.BOOLEAN,
    "should_remove_source_branch": bigquery.enums.SqlTypeNames.BOOLEAN,
    "force_remove_source_branch": bigquery.enums.SqlTypeNames.BOOLEAN,
    "reference": bigquery.enums.SqlTypeNames.STRING,
    # "references",
    "refs_short": bigquery.enums.SqlTypeNames.STRING,
    "refs_relative": bigquery.enums.SqlTypeNames.STRING,
    "refs_full": bigquery.enums.SqlTypeNames.STRING,
    # # "time_stats",
    "ts_time_estimate": bigquery.enums.SqlTypeNames.FLOAT,
    "ts_total_time_spent": bigquery.enums.SqlTypeNames.FLOAT,
    "ts_human_time_estimate": bigquery.enums.SqlTypeNames.FLOAT,
    "ts_human_total_time_spent": bigquery.enums.SqlTypeNames.FLOAT,
    "squash": bigquery.enums.SqlTypeNames.INTEGER,
    # # "task_completion_status",
    "tcs_count": bigquery.enums.SqlTypeNames.INTEGER,
    "tcs_completed_count": bigquery.enums.SqlTypeNames.INTEGER,
    "has_conflicts": bigquery.enums.SqlTypeNames.BOOLEAN,
    "blocking_discussions_resolved": bigquery.enums.SqlTypeNames.BOOLEAN,
    "approvals_before_merge": bigquery.enums.SqlTypeNames.BOOLEAN,
    "jira": bigquery.enums.SqlTypeNames.STRING,
    "jira_id": bigquery.enums.SqlTypeNames.STRING,
    "branch_jira": bigquery.enums.SqlTypeNames.STRING,
    "stale": bigquery.enums.SqlTypeNames.BOOLEAN,
    "merged": bigquery.enums.SqlTypeNames.BOOLEAN,
    "time_since": bigquery.enums.SqlTypeNames.FLOAT,
    "time": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "project": bigquery.enums.SqlTypeNames.STRING,
    "mr_id": bigquery.enums.SqlTypeNames.STRING,
    "ref": bigquery.enums.SqlTypeNames.STRING,
    "authorname": bigquery.enums.SqlTypeNames.STRING,
    "pi": bigquery.enums.SqlTypeNames.INTEGER,
    "sprint": bigquery.enums.SqlTypeNames.INTEGER,
    "hp_id": bigquery.enums.SqlTypeNames.INTEGER,
    "hp_status": bigquery.enums.SqlTypeNames.STRING,
    "hp_created_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "hp_updated_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "last_commit_name": bigquery.enums.SqlTypeNames.STRING,
    "last_commit_date": bigquery.enums.SqlTypeNames.TIMESTAMP,
}

PIPELINE_KEYS = {
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "iid": bigquery.enums.SqlTypeNames.INTEGER,
    "project_id": bigquery.enums.SqlTypeNames.INTEGER,
    "sha": bigquery.enums.SqlTypeNames.STRING,
    "ref": bigquery.enums.SqlTypeNames.STRING,
    "status": bigquery.enums.SqlTypeNames.STRING,
    "source": bigquery.enums.SqlTypeNames.STRING,
    "created_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "updated_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "web_url": bigquery.enums.SqlTypeNames.STRING,
    "before_sha": bigquery.enums.SqlTypeNames.STRING,
    "yaml_errors": bigquery.enums.SqlTypeNames.STRING,
    # "user",
    "user_username": bigquery.enums.SqlTypeNames.STRING,
    "user_name": bigquery.enums.SqlTypeNames.STRING,
    "user_state": bigquery.enums.SqlTypeNames.STRING,
    "user_avatar_url": bigquery.enums.SqlTypeNames.STRING,
    "user_web_url": bigquery.enums.SqlTypeNames.STRING,
    "started_at": bigquery.enums.SqlTypeNames.STRING,
    "finished_at": bigquery.enums.SqlTypeNames.STRING,
    "committed_at": bigquery.enums.SqlTypeNames.STRING,
    "duration": bigquery.enums.SqlTypeNames.FLOAT,
    "queued_duration": bigquery.enums.SqlTypeNames.FLOAT,
    "coverage": bigquery.enums.SqlTypeNames.FLOAT,
    # # "detailed_status",
    "ds_icon": bigquery.enums.SqlTypeNames.STRING,
    "ds_text": bigquery.enums.SqlTypeNames.STRING,
    "ds_label": bigquery.enums.SqlTypeNames.STRING,
    "ds_group": bigquery.enums.SqlTypeNames.STRING,
    "ds_tooltip": bigquery.enums.SqlTypeNames.STRING,
    "ds_has_details": bigquery.enums.SqlTypeNames.BOOLEAN,
    "ds_details_path": bigquery.enums.SqlTypeNames.STRING,
    "ds_illustration": bigquery.enums.SqlTypeNames.STRING,
    "ds_favicon": bigquery.enums.SqlTypeNames.STRING,
    "time": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "project": bigquery.enums.SqlTypeNames.STRING,
    "pipeline_id": bigquery.enums.SqlTypeNames.STRING,
    "created_by": bigquery.enums.SqlTypeNames.STRING,
    "passed": bigquery.enums.SqlTypeNames.BOOLEAN,
    "last_run": bigquery.enums.SqlTypeNames.FLOAT,
    "test_count": bigquery.enums.SqlTypeNames.INTEGER,
    "test_failed": bigquery.enums.SqlTypeNames.INTEGER,
    "test_error": bigquery.enums.SqlTypeNames.INTEGER,
    "test_success": bigquery.enums.SqlTypeNames.INTEGER,
    "test_skipped": bigquery.enums.SqlTypeNames.INTEGER,
    "pass_rate": bigquery.enums.SqlTypeNames.FLOAT,
    "pi": bigquery.enums.SqlTypeNames.INTEGER,
    "sprint": bigquery.enums.SqlTypeNames.INTEGER,
}

gl_token = os.getenv('GITLAB_TOKEN')
GITLAB_URL = "https://gitlab.com"
gl = gitlab.Gitlab(url=GITLAB_URL, private_token=gl_token, api_version='4')

try:
    gl.auth()
except gitlab.GitlabAuthenticationError:
    log.error("Authentication error: did you provide a valid token?")
    sys.exit(1)
except gitlab.GitlabGetError as err:
    response = json.loads(err.response_body.decode())
    log.error("Problem contacting Gitlab: {}".format(response["message"]))
    sys.exit(1)


def load_table_dataframe(
    table_df: pandas.DataFrame, table_keys: dict, table_id: str
) -> "bigquery.Table":
    """dynamically load a dataframe into big query"""

    # Construct a BigQuery client object.
    client = bigquery.Client()

    for k in table_keys.keys():
        if table_keys[k] == bigquery.enums.SqlTypeNames.TIMESTAMP:
            # table_df[k].fillna("1970-01-01 00:00:00")
            table_df[k] = table_df[k].apply(lambda x: "1970-01-01 00:00:00" if ((isinstance(x, str) and (x.isspace() or x == 'None' or not x))
                                or x == None) else x)
            table_df[k] = table_df[k].apply(str).apply(pandas.Timestamp)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.INTEGER:
            table_df[k] = table_df[k].astype(int)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.BOOLEAN:
            table_df[k] = table_df[k].astype(bool)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.FLOAT:
            table_df[k] = table_df[k].astype(float)
        else:
            table_df[k] = table_df[k].astype(str)

    df_schema = []
    for col in table_df.columns:
        if col in table_keys:
            df_schema.append(bigquery.SchemaField(col, table_keys[col]))

    job_config = bigquery.LoadJobConfig(
        # Specify a (partial) schema. All columns are always written to the
        # table. The schema is used to assist in data type definitions.
        schema=df_schema,
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        write_disposition="WRITE_TRUNCATE",
    )

    job = client.load_table_from_dataframe(
        table_df[table_keys.keys()], table_id, job_config=job_config
    )  # Make an API request.
    job.result()  # Wait for the job to complete.

    table = client.get_table(table_id)  # Make an API request.
    log.info(
        "Loaded {} rows and {} columns to {}".format(
            table.num_rows, len(table.schema), table_id
        )
    )
    # [END bigquery_load_table_dataframe]
    return table


def get_mr(project_id, mr_iid, log):
    """ Get the full Merge Request details """
    if project_id in mr_extended_state and mr_iid in mr_extended_state[project_id]:
        log.debug("hit: %s - %s", project_id, mr_iid)
        return mr_extended_state[project_id][mr_iid]
    log.debug("miss: %s - %s", project_id, mr_iid)

    url = (
        f"/projects/{project_id}"
        f"/merge_requests/{mr_iid}"
    )
    log.debug("merge request list url: %s", url)

    try:
        merge_requests = gl.http_get(url)
        # log.info("merge request details: %s", merge_requests)
        if merge_requests['state'] == "merged" or merge_requests['state'] == "closed":
            if not project_id in mr_extended_state:
                mr_extended_state[project_id] = {}
            mr_extended_state[project_id][mr_iid] = merge_requests
        return merge_requests
    except gitlab.exceptions.GitlabHttpError as e:
        log.error("get on MR: %s failed: %s", url, e)
    return {}


# GET /projects/:id/merge_requests/:merge_request_iid/commits

def get_mr_commits(project_id, mr_iid, state, log):
    """ Get the Merge Request commit details """
    if project_id in mr_commit_state and mr_iid in mr_commit_state[project_id]:
        log.debug("hit: %s - %s", project_id, mr_iid)
        return mr_commit_state[project_id][mr_iid]
    log.debug("miss: %s - %s", project_id, mr_iid)

    url = (
        f"/projects/{project_id}"
        f"/merge_requests/{mr_iid}/commits"
    )
    log.debug("merge request commit list url: %s", url)

    try:
        merge_commits = gl.http_get(url)
        log.debug("merge commit details: %s", merge_commits)
        if state == "merged" or state == "closed":
            if not project_id in mr_commit_state:
                mr_commit_state[project_id] = {}
            mr_commit_state[project_id][mr_iid] = merge_commits
        return merge_commits
    except gitlab.exceptions.GitlabHttpError as e:
        log.error("get on MR: %s failed: %s", url, e)
    return []

def save_mr_extended_state():
    """Pickle the mr extended state to cache"""
    with open(mr_state_extended_file, "wb") as fle:
        pickle.dump(mr_extended_state, fle)
        fle.flush()

def save_mr_commit_state():
    """Pickle the mr commit state to cache"""
    with open(mr_state_commit_file, "wb") as fle:
        pickle.dump(mr_commit_state, fle)
        fle.flush()



@click.command()
@click.option(
    "--skipprojects",
    envvar="SKIP_PROJECTS",
    is_flag=True,
    default=False,
    help="Skip processing projects",
)
@click.option(
    "--skipbranches",
    envvar="SKIP_BRANCHES",
    is_flag=True,
    default=False,
    help="Skip processing branches",
)
@click.option(
    "--skipmrs",
    envvar="SKIP_MRS",
    is_flag=True,
    default=False,
    help="Skip processing mrs",
)
@click.option(
    "--skippipelines",
    envvar="SKIP_PIPELINES",
    is_flag=True,
    default=False,
    help="Skip processing pipelines",
)
@click.option(
    "--skipupdate",
    envvar="SKIP_UPDATE",
    is_flag=True,
    default=False,
    help="Skip DB update",
)
@click.option(
    "--selectedpi",
    envvar="PI",
    default=1,
    show_default=True,
    help="Selected PI",
)
def main(
    skipprojects: bool = False,
    skipbranches: bool = False,
    skipmrs: bool = False,
    skippipelines: bool = False,
    skipupdate: bool = False,
    selectedpi: int = 1
):

    # data accumulators
    projects_df = pandas.DataFrame()
    branches_df = pandas.DataFrame()
    mrs_df = pandas.DataFrame()
    pipelines_df = pandas.DataFrame()
    groups_df = pandas.DataFrame()


    # calculate PI boundaries
    selected_pi = selectedpi
    first_pi = 1
    select_pis = 5 + 1
    this_pi = datetime.strptime("2018-12-12", "%Y-%m-%d").replace(tzinfo=timezone.utc)
    the_pis = []
    last_start = ""
    for pi in range(first_pi, selected_pi + 1):
        end_date = this_pi + timedelta(days=(13 * 7) + 1)
        if pi == 10:
            end_date = end_date - timedelta(days=3)
            this_pi = this_pi - timedelta(days=2)
        if pi == 14:
            this_pi = this_pi - timedelta(days=3)
        if pi == 15:
            end_date = end_date - timedelta(days=5)
            this_pi = this_pi - timedelta(days=4)
        if pi == 16:
            end_date = end_date - timedelta(days=1)
        if pi == 17:
            end_date = end_date - timedelta(days=1)
        if pi == 18:
            end_date = end_date - timedelta(days=1)
        if pi == 19:
            end_date = end_date - timedelta(days=1)
        if pi == 20:
            end_date = end_date - timedelta(days=1)
        if pi == 21:
            end_date = end_date - timedelta(days=1)
        if pi == 22:
            end_date = end_date - timedelta(days=1)
        sp_start = this_pi
        sprints = []
        for sp in range(1, 6, 1):
            sp_end = (sp_start + timedelta(days=14)) - timedelta(seconds=1)
            sprints.append(
                {'sprint': sp,
                 'start_time': sp_start,
                 'start': sp_start.strftime("%Y-%m-%dT%H:%M:%SZ"),
                 'finish_time': sp_end,
                 'finish': sp_end.strftime("%Y-%m-%dT%H:%M:%SZ")
                 })
            sp_start = sp_end + timedelta(seconds=1)
        # last sprint goes to the end of PI
        sprints.append(
            {'sprint': 6,
             'start_time': sp_start,
             'start': sp_start.strftime("%Y-%m-%dT%H:%M:%SZ"),
             'finish_time': (end_date - timedelta(seconds=1)),
             'finish': (end_date - timedelta(seconds=1)).strftime("%Y-%m-%dT%H:%M:%SZ")
            })

        the_pis.append(
            {
                "pi": "PI{}".format(pi),
                "id": pi,
                "start": this_pi.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "started": this_pi.strftime("%Y-%m-%d"),
                "start_time": this_pi,
                "finish": (end_date - timedelta(seconds=1)).strftime("%Y-%m-%dT%H:%M:%SZ"),
                "finish_time": (end_date - timedelta(seconds=1)),
                'sprints': sprints
            }
        )
        last_start = this_pi
        this_pi = end_date

    last_start = last_start.strftime("%Y-%m-%dT%H:%M:%SZ")

    pis = the_pis.copy()
    current_pi = pis.pop(-1)
    log.info("Current PI: " + str(current_pi))
    log.info("Last start: " + last_start)

    def which_pi_and_sprint(dt):
        """ Which PI is this? """
        for pi in the_pis:
            if dt >= pi['start_time'] and dt <= pi['finish_time']:
                for sp in pi['sprints']:
                    if dt >= sp['start_time'] and dt <= sp['finish_time']:
                        log.debug("[1] pi and sprint: %s => %d / %d", dt, pi['id'], sp['sprint'])
                        return pi['id'], sp['sprint']
                log.debug("[2] pi and sprint: %s => %d / %d", dt, pi['id'], 1)
                return pi['id'], 1
        log.debug("[3] pi and sprint: %s => %d / %d", dt, 1, 1)
        return 1, 1

    def which_pi(dt):
        """ Which PI is this? """
        for pi in the_pis:
            if dt >= pi['start_time'] and dt <= pi['finish_time']:
                for sp in pi['sprints']:
                    if dt >= sp['start_time'] and dt <= sp['finish_time']:
                        log.debug("[1] pi and sprint: %s => %d / %d", dt, pi['id'], sp['sprint'])
                        return pi['id']
                log.debug("[2] pi and sprint: %s => %d / %d", dt, pi['id'], 1)
                return pi['id']
        log.debug("[3] pi and sprint: %s => %d / %d", dt, 1, 1)
        return 1

    def which_sprint(dt):
        """ Which PI is this? """
        for pi in the_pis:
            if dt >= pi['start_time'] and dt <= pi['finish_time']:
                for sp in pi['sprints']:
                    if dt >= sp['start_time'] and dt <= sp['finish_time']:
                        log.debug("[1] pi and sprint: %s => %d / %d", dt, pi['id'], sp['sprint'])
                        return sp['sprint']
                log.debug("[2] pi and sprint: %s => %d / %d", dt, pi['id'], 1)
                return 1
        log.debug("[3] pi and sprint: %s => %d / %d", dt, 1, 1)
        return 1

    # Projects
    if not skipprojects:
        seen = []
        log.info("Processing projects: %d", len(project_state.keys()))
        for measure in project_state.values():
            if measure['path_with_namespace'] not in seen:
                dfrow = {k: measure[k] for k in PROJECT_KEYS.keys() if k in measure}
                projects_df = pandas.concat(
                    [projects_df, pandas.DataFrame([dfrow])], ignore_index=True
                )
                seen.append(measure['path_with_namespace'])

        projects_df["time"] = projects_df["time"].apply(str).apply(pandas.Timestamp)
        projects_df["pi"] = projects_df["time"].apply(which_pi)
        projects_df["sprint"] = projects_df["time"].apply(which_sprint)


        # have to use 'project' as 'id' is an integer and unsupported by bigquery
        projects_df.set_index("project")
        log.info(projects_df)
        if not skipupdate:
            load_table_dataframe(
                projects_df, PROJECT_KEYS, "jira-reporting-poc.gitlabmetrics.projects"
            )
        log.info("Processed projects (cols): %d", len(projects_df.keys()))

    # MRs
    if not skipmrs:
        log.info("Processing mrs (projects): %d", len(mr_state.keys()))
        cnt = 0
        mrs = list()
        mr_cnt = 0
        for key in mr_state.keys():
            # cnt += 1
            # if cnt > 1:
            #     break

            project = mr_state[key]

            for mr in project.keys():
                measure = project[mr]
                # 'updated_at': '2022-12-08T10:05:20.348Z'
                # created_since = (datetime.now() - timedelta(days=90)).strftime("%Y-%m-%dT%H:%M:%S")
                log.debug("MR: %s", measure)
                mr_detail = get_mr(measure['project_id'], measure['iid'], log)
                log.debug("MR: %s", mr_detail)
                mr_commits = get_mr_commits(measure['project_id'], measure['iid'], (mr_detail['state'] if 'state' in mr_detail else 'unknown'), log)
                log.debug("MR COMMITS: %s", mr_commits)
                mr_last_commit = None
                if mr_commits:
                    mr_last_commit = mr_commits[0]
                log.debug("MR LAST COMMIT: %s", mr_last_commit)
                dfrow = {k: measure[k] for k in MR_KEYS.keys() if k in measure}
                m = jira_regex.search(str(dfrow["title"]))
                jira_id = ""
                if m:
                    jira_id = m.group(1)
                dfrow["jira_id"] = jira_id.upper()
                if measure["merge_user"]:
                    for k in measure["merge_user"]:
                        dfrow["mu_" + k] = measure["merge_user"][k]
                if measure["assignee"]:
                    for k in measure["assignee"]:
                        dfrow["au_" + k] = measure["assignee"][k]
                for k in measure["time_stats"]:
                    dfrow["ts_" + k] = measure["time_stats"][k]
                for k in measure["task_completion_status"]:
                    dfrow["tcs_" + k] = measure["task_completion_status"][k]
                for k in measure["references"]:
                    dfrow["refs_" + k] = measure["references"][k]
                dfrow["hp_id"] = 0
                dfrow["hp_status"] = "Unknown"
                dfrow["hp_created_at"] = ""
                dfrow["hp_updated_at"] = ""
                if "head_pipeline" in mr_detail and mr_detail["head_pipeline"]:
                    for k in mr_detail["head_pipeline"]:
                        dfrow["hp_" + k] = mr_detail["head_pipeline"][k]
                if mr_last_commit:
                    dfrow['last_commit_name'] = mr_last_commit['committer_name']
                    dfrow['last_commit_date'] = mr_last_commit['committed_date']

                mrs.append(dfrow)
                mr_cnt += 1

                if not mr_cnt % 1000:
                    log.info("MRs: accumulated: %d", mr_cnt)
                    save_mr_extended_state()
                    save_mr_commit_state()

        # mrs_df = pandas.concat(
        #     [mrs_df, pandas.DataFrame([dfrow])], ignore_index=True
        # )
        save_mr_extended_state()
        save_mr_commit_state()
        log.info("MRs: finished collecting: %d", mr_cnt)
        mrs_df = pandas.DataFrame.from_records(mrs)

        mrs_df["time"] = mrs_df["time"].apply(str).apply(pandas.Timestamp)
        log.info("time : %s", str(mrs_df["time"]))
        mrs_df["pi"] = mrs_df["time"].apply(which_pi)
        mrs_df["sprint"] = mrs_df["time"].apply(which_sprint)

        mrs_df.set_index("mr_id")
        log.info(mrs_df)
        if not skipupdate:
            load_table_dataframe(
                mrs_df, MR_KEYS, "jira-reporting-poc.gitlabmetrics.mergerequests"
            )
        log.info("MRs: finished loading: %d", mr_cnt)

    # Branches
    if not skipbranches:
        branches = list()
        branch_cnt = 0
        log.info("Processing branches: %d", len(branch_state.keys()))
        for key in branch_state.keys():
            project = branch_state[key]

            for branch in project.keys():
                measure = project[branch]
                dfrow = {k: measure[k] for k in BRANCH_KEYS.keys() if k in measure}
                m = jira_regex.search(str(dfrow["name"]))
                jira_id = ""
                if m:
                    jira_id = m.group(1)
                dfrow["jira_id"] = jira_id.upper()

                # branches_df = pandas.concat(
                #     [branches_df, pandas.DataFrame([dfrow])], ignore_index=True
                # )
                branches.append(dfrow)
                branch_cnt += 1
                if not branch_cnt % 1000:
                    log.info("Branches: accumulated: %d", branch_cnt)

        log.info("Branches: finished collecting: %d", branch_cnt)
        branches_df = pandas.DataFrame.from_records(branches)

        branches_df["time"] = branches_df["time"].apply(str).apply(pandas.Timestamp)
        branches_df["pi"] = branches_df["time"].apply(which_pi)
        branches_df["sprint"] = branches_df["time"].apply(which_sprint)

        # have to use 'project' as 'id' is an integer and unsupported by bigquery
        branches_df.set_index("project")
        log.info(branches_df)
        if not skipupdate:
            load_table_dataframe(
                branches_df, BRANCH_KEYS, "jira-reporting-poc.gitlabmetrics.branches"
            )
        log.info("Branches: finished loading: %d", branch_cnt)

    # Pipelines
    if not skippipelines:
        pipelines = list()
        pipeline_cnt = 0
        log.info("Processing pipelines: %d", len(pipeline_state.keys()))
        for key in pipeline_state.keys():
            project = pipeline_state[key]

            for pipeline in project.keys():
                measure = project[pipeline]
                dfrow = {k: measure[k] for k in PIPELINE_KEYS.keys() if k in measure}
                if measure["user"]:
                    for k in measure["user"]:
                        dfrow["user_" + k] = measure["user"][k]
                for k in measure["detailed_status"]:
                    dfrow["ds_" + k] = measure["detailed_status"][k]
                # pipelines_df = pandas.concat(
                #     [pipelines_df, pandas.DataFrame([dfrow])], ignore_index=True
                # )
                pipelines.append(dfrow)
                pipeline_cnt += 1
                if not pipeline_cnt % 1000:
                    log.info("Pipelines: accumulated: %d", pipeline_cnt)

        log.info("Pipelines: finished collecting: %d", pipeline_cnt)
        pipelines_df = pandas.DataFrame.from_records(pipelines)

        pipelines_df["time"] = pipelines_df["time"].apply(str).apply(pandas.Timestamp)
        pipelines_df["pi"] = pipelines_df["time"].apply(which_pi)
        pipelines_df["sprint"] = pipelines_df["time"].apply(which_sprint)

        pipelines_df.set_index("pipeline_id")
        log.info(pipelines_df)
        if not skipupdate:
            load_table_dataframe(
                pipelines_df, PIPELINE_KEYS, "jira-reporting-poc.gitlabmetrics.pipelines"
            )
        log.info("Pipelines: finished loading: %d", pipeline_cnt)

    # all done.
    log.info("all done.")
    sys.exit()


if __name__ == "__main__":
    main()
