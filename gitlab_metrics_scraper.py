#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Convenience wrapper for running gitlab_metrics directly from source tree."""


from ska_cicd_gitlab_metrics.gitlab_metrics import main

if __name__ == "__main__":
    main()
