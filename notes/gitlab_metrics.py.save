# -*- coding: utf-8 -*-

"""ska.gitlab_metrics.gitlab_metrics: main launcher of metrics app"""

import asyncio
import click
import logging
import os
from voluptuous import MultipleInvalid
import yaml

from .config import GitLabPrometheusScraperConfig
from .scraper import GitLabPrometheusScraper

logging_level = os.environ.get("LOGGING_LEVEL", "ERROR")
logging_format = "%(asctime)s [level=%(levelname)s] [thread=%(threadName)s] " \
                 "[module=%(module)s] [line=%(lineno)d]: %(message)s"
logging.basicConfig(level=logging_level, format=logging_format)
log = logging.getLogger('gitlab_metrics')
log.debug("Logging level is: {debug}".format(debug=logging_level))
logging.getLogger("asyncio").setLevel(logging.ERROR)
logging.getLogger("aiohttp").setLevel(logging.ERROR)


def validate_config_file(ctx, param, value):
    config = GitLabPrometheusScraperConfig()
    try:
        return config.config_schema(yaml.load(value, Loader=yaml.FullLoader))
    except yaml.YAMLError as e:
        raise click.BadParameter(str(e), ctx=ctx, param=param)
    except MultipleInvalid as e:
        raise click.BadParameter(e.msg, ctx=ctx, param=param, param_hint=e.path)


@click.command()
@click.argument(
    "config",
    envvar="CONFIG_PATH",
    callback=validate_config_file,
    type=click.File("r"),
    default="gitlab-metrics.yml",
)
def main(config: dict = None):
    # over ride token with ENV VAR
    if os.environ.get('GITLAB_TOKEN'):
        config["global"]['gitlab_token'] = os.environ.get('GITLAB_TOKEN')
    if os.environ.get('RTD_TOKEN'):
        config["global"]['rtd_token'] = os.environ.get('RTD_TOKEN')
    log.debug("Loaded config: {}".format(config))
    app = GitLabPrometheusScraper(config=config)
    loop = asyncio.get_event_loop()
    loop.create_task(app.start())
    loop.create_task(app.start_sub_groups())
    loop.create_task(app.start_projects())
    loop.create_task(app.start_project_update_metrics())
    loop.create_task(app.start_branch_update_metrics())
    loop.create_task(app.start_merge_request_update_metrics())
    loop.create_task(app.start_commit_update_metrics())
    # loop.create_task(app.start_metrics_output())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(app.stop())
    loop.close()
