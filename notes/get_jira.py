#!/usr/bin/env python

import asyncio
import aiojira
import sys
import time
from datetime import datetime, timedelta
import os

JIRA_URL = 'https://jira.skatelescope.org/'
USER = 'P.Harding'
IGNORE_PROJECTS = ['AAVS', 'AIV', 'AUTO', 'CHR', 'GTP', 'HQIT', 'IFID', 'L1', 'L2', 'L3',
                   'OMCPT', 'TES', 'SKB', 'SPO', 'SP', 'SPP', 'SS', 'SSMT', 'SSS', 'SWAT2', 'TRKECP', 'VE', 'XR']

j_token = os.getenv('JIRA_TOKEN')

# based on https://pypi.org/project/aiojira/
class SKAJira:
    """ SKA Jira encapsulation """

    def __init__(self, config: dict = {}):
        self.config = config
        self.token = os.getenv('JIRA_TOKEN') if os.getenv('JIRA_TOKEN') else config['jira_token']
        self.user = config['user']
        self.jira_url = config['jira_url'] if 'jira_url' in config else JIRA_URL
        options = {'server': self.jira_url}
        self.jira = asyncio.run(aiojira.JIRA.create(
            server=self.jira_url, basic_auth=(self.user, self.token)))
        # self.jira = jira.JIRA(options, basic_auth=(self.user, self.token))

    async def get_projects(self, projectType: str = None):
        """ Get the list of allowed project by type """
        projects = await self.jira.projects()
        projects = [p for p in projects if not projectType or p.projectTypeKey == projectType]
        # print(dir(projects[0]))
        # print(projects[0].raw)
        return [{'key': p.key, 'id': int(p.id), 'name': p.name, 'type': p.projectTypeKey} for p in projects if p.key not in IGNORE_PROJECTS]

    async def get_defects(self, project: str, dt: str):
        """ Get the list of Defects from a given date """
        query = 'project={} and created > {} and type=Defect'
        issues = [await self.jira.issue(x) for x in await self.jira.search_issues(query.format(project, dt))]
        return [{'key': x.key,  'summary': x.fields.summary, 'type': 'Defect', 'created': x.fields.created, 'updated': x.fields.updated} for x in issues]

    async def get_lead_time(self, project: str, dt: str):
        """ Get the lead time for a list of Stories from a given date """
        query = 'project={} and created > {} and status=Done and type=Story'
        issues = [await self.jira.issue(x) for x in await self.jira.search_issues(query.format(project, dt))]
        return [{'key': x.key,  'summary': x.fields.summary, 'type': 'Story', 'created': x.fields.created, 'updated': x.fields.updated} for x in issues]


jcon = SKAJira({'user': USER, 'jira_token': j_token})
print(jcon)
print(dir(jcon))
projects = asyncio.run(jcon.get_projects("software"))
# print(projects)
for p in sorted(projects, key=lambda p: p['key']):
    print("{key:10} {name} [{id:n}]".format(key=p['key']+':', id=p['id'], name=p['name']))

created_since = (datetime.now() - timedelta(days=180)).strftime("%Y-%m-%d")
print("Searching from: {}".format(created_since))
res = asyncio.run(jcon.get_defects("ST", created_since))
print(res)
res = asyncio.run(jcon.get_lead_time("ST", created_since))
print(res)

sys.exit(0)
