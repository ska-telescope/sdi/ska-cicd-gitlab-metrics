#!/usr/bin/env python

import gitlab
import sys
import os

GITLAB_URL = 'https://gitlab.com/'
project_name = 'piersharding/dask-operator'
branch_name = 'master'

gl_token = os.getenv('GITLAB_TOKEN')

gl = gitlab.Gitlab(GITLAB_URL, private_token=gl_token, api_version=4)

try:
    gl.auth()
except GitlabAuthenticationError:
    log.error('Authentication error: did you provide a valid token?')
    sys.exit(1)
except GitlabGetError as err:
    response = json.loads(err.response_body.decode())
    log.error('Problem contacting Gitlab: {}'
        .format(response['message']))
    sys.exit(1)

print("ok")

project = gl.projects.get(project_name)
branch = project.branches.get(branch_name)
top_commit = project.commits.get(branch.commit['short_id'])
pipelines = project.pipelines.list()



# print("\n\nProject:")
# print("\n\nProject[{}]:".format(project_name))
# print(project)
# print("\n\nBranch[{}]:".format(branch_name))
# print(branch)
# print("\n\nTop Commit:")
# print(top_commit)

first = pipelines.pop(0)
pipeline = project.pipelines.get(first.id)

# print("\n\nFirst Pipeline: {}".format(first))

# last pipeline status, coverage
print("\n\nThe Pipeline: {}".format(pipeline))

test_report_url = "{}api/v4/projects/{}/pipelines/{}/test_report".format(GITLAB_URL,project.id, pipeline.id)
print("\n\nThe URL: {}".format(test_report_url))
res = gl.http_get(test_report_url)

print(dir(res))
print(type(res))
print(res)

