#!/usr/bin/env python

import aiohttp
import asyncio
import gidgetlab.aiohttp
import sys
import os
import json

GITLAB_URL = "https://gitlab.com/"
GITLAB_USER = "ska-telescope"
PER_PAGE = 1000
project_name = "piersharding/dask-operator"
branch_name = "master"

gl_token = os.getenv("GITLAB_TOKEN")

# https://gitlab.com/api/v4/projects/19278002


async def main_projects():
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        # Make your requests, e.g. ...
        url = "{base_url}api/v4/groups/{gitlab_user}/projects?simple=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        projects = []
        for p in data:
            url = "{base_url}api/v4/projects/{project:d}?license=true".format(
                base_url=GITLAB_URL, project=p["id"]
            )
            projects.append(await gl.getitem(url))
            break
        return projects


async def sub_group_projects(subgroup):
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        # Make your requests, e.g. ...
        url = "{base_url}api/v4/groups/{sub_group:d}/projects?simple=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, sub_group=subgroup, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        return data


async def sub_groups():
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        # Make your requests, e.g. ...
        url = "{base_url}api/v4/groups/{gitlab_user}/subgroups?simple=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        subgroups = [g["id"] for g in data]
        return subgroups


async def main():
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        # Make your requests, e.g. ...
        url = "{base_url}api/v4/groups/{gitlab_user}/projects?simple=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        return data


data = asyncio.run(sub_groups())
print(data)

data = asyncio.run(main_projects())
print(json.dumps(data, indent=4, sort_keys=True))

sys.exit(0)

gl = gitlab.Gitlab(GITLAB_URL, private_token=gl_token, api_version=4)

try:
    gl.auth()
except GitlabAuthenticationError:
    log.error("Authentication error: did you provide a valid token?")
    sys.exit(1)
except GitlabGetError as err:
    response = json.loads(err.response_body.decode())
    log.error("Problem contacting Gitlab: {}".format(response["message"]))
    sys.exit(1)

print("ok")

project = gl.projects.get(project_name)
branch = project.branches.get(branch_name)
top_commit = project.commits.get(branch.commit["short_id"])
pipelines = project.pipelines.list()


# print("\n\nProject:")
# print("\n\nProject[{}]:".format(project_name))
# print(project)
# print("\n\nBranch[{}]:".format(branch_name))
# print(branch)
# print("\n\nTop Commit:")
# print(top_commit)

first = pipelines.pop(0)
pipeline = project.pipelines.get(first.id)

# print("\n\nFirst Pipeline: {}".format(first))

# last pipeline status, coverage
print("\n\nThe Pipeline: {}".format(pipeline))

test_report_url = "{}api/v4/projects/{}/pipelines/{}/test_report".format(
    GITLAB_URL, project.id, pipeline.id
)
print("\n\nThe URL: {}".format(test_report_url))
res = gl.http_get(test_report_url)

print(dir(res))
print(type(res))
print(res)
