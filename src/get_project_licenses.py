#!/usr/bin/env python3

import asyncio
import csv
import logging
import os
import sys
from datetime import datetime, timedelta, timezone

import aiohttp
import click
import gidgetlab.aiohttp
import pandas
import spdx_lookup as lookup
from google.cloud import bigquery
from spdx_lookup import LicenseMatch

LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "ERROR").upper()
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] [thread=%(threadName)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("load-bigquery")

GITLAB_URL = "https://gitlab.com/"
GITLAB_USER = "ska-telescope"
# GITLAB_USER = "piersharding"
PER_PAGE = 1000
project_name = "piersharding/dask-operator"
branch_name = "master"

gl_token = os.getenv("GITLAB_TOKEN")

EXT_PROJECT_KEYS = []
PROJECTS = {}

PROJECT_KEYS = {
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "project": bigquery.enums.SqlTypeNames.STRING,
    "created_at": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "group": bigquery.enums.SqlTypeNames.STRING,
    "art": bigquery.enums.SqlTypeNames.STRING,
    "archived": bigquery.enums.SqlTypeNames.STRING,
    "default_branch": bigquery.enums.SqlTypeNames.STRING,
    "web_url": bigquery.enums.SqlTypeNames.STRING,
    "license_url": bigquery.enums.SqlTypeNames.STRING,
    "author_email": bigquery.enums.SqlTypeNames.STRING,
    "author_name": bigquery.enums.SqlTypeNames.STRING,
    "license_key": bigquery.enums.SqlTypeNames.STRING,
    "shortname": bigquery.enums.SqlTypeNames.STRING,
    "name": bigquery.enums.SqlTypeNames.STRING,
    "short_text": bigquery.enums.SqlTypeNames.STRING,
    "scan_license": bigquery.enums.SqlTypeNames.STRING,
    "scan_confidence": bigquery.enums.SqlTypeNames.FLOAT,
}


with open("./existing-projects.csv", encoding="utf-8") as csvfile:
    projectreader = csv.DictReader(csvfile, delimiter=",")
    for row in projectreader:
        if not row["id"] == "Count":
            row["id"] = int(row["id"])
            EXT_PROJECT_KEYS.append(row["id"])
            PROJECTS[row["id"]] = row
            log.debug("[PROJECTS] id: %d => %s", row["id"], row)

log.info("[EXT_PROJECT_KEYS] count: %d", len(EXT_PROJECT_KEYS))
log.debug("[EXT_PROJECT_KEYS] %s", EXT_PROJECT_KEYS)


# sys.exit()
# https://gitlab.com/api/v4/projects/19278002


def load_table_dataframe(
    table_df: pandas.DataFrame, table_keys: dict, table_id: str
) -> "bigquery.Table":
    """dynamically load a dataframe into big query"""

    # Construct a BigQuery client object.
    client = bigquery.Client()

    for k in table_keys.keys():
        log.debug("table: %s key: %s", table_id, k)
        log.debug("column: %s", table_df[k])
        if table_keys[k] == bigquery.enums.SqlTypeNames.TIMESTAMP:
            # table_df[k].fillna("1970-01-01 00:00:00")
            table_df[k] = table_df[k].apply(
                lambda x: (
                    "1970-01-01 00:00:00"
                    if (
                        (isinstance(x, str) and (x.isspace() or x == "None" or not x))
                        or x is None
                    )
                    else x
                )
            )
            table_df[k] = table_df[k].apply(str).apply(pandas.Timestamp)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.INTEGER:
            table_df[k] = table_df[k].astype(int)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.BOOLEAN:
            table_df[k] = table_df[k].astype(bool)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.FLOAT:
            table_df[k] = table_df[k].astype(float)
        else:
            table_df[k] = table_df[k].astype(str)

    df_schema = []
    for col in table_df.columns:
        if col in table_keys:
            df_schema.append(bigquery.SchemaField(col, table_keys[col]))

    job_config = bigquery.LoadJobConfig(
        # Specify a (partial) schema. All columns are always written to the
        # table. The schema is used to assist in data type definitions.
        schema=df_schema,
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        # alternately - WRITE_APPEND
        write_disposition="WRITE_TRUNCATE",
    )

    job = client.load_table_from_dataframe(
        table_df[table_keys.keys()], table_id, job_config=job_config
    )  # Make an API request.
    job.result()  # Wait for the job to complete.

    table = client.get_table(table_id)  # Make an API request.
    log.info(
        "Loaded {} rows and {} columns to {}".format(
            table.num_rows, len(table.schema), table_id
        )
    )
    # [END bigquery_load_table_dataframe]
    return table


async def main_projects():
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        # Make your requests, e.g. ...
        url = "{base_url}api/v4/groups/{gitlab_user}/projects?simple=true&license=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        return data


async def sub_group_projects(subgroup):
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        res = []
        try:
            thispage = 0
            per_page = 100
            call_counter = 0
            url = "{base_url}api/v4/groups/{sub_group:d}/projects?simple=false&license=true&per_page={per_page:d}".format(
                base_url=GITLAB_URL, sub_group=subgroup, per_page=PER_PAGE
            )
            while True:
                # Keep going until results less than a page
                thispage += 1
                this_url = f"{url}&per_page={per_page:d}&page={thispage:d}"
                data = await gl.getitem(this_url)
                log.info("[call_api] received: %d", len(data))
                call_counter += 1
                # save results and check to go round again
                res.extend(data)
                if len(data) < per_page:
                    break
        except aiohttp.client_exceptions.ClientConnectorError as err:
            log.error("[call_api] exception: %s", str(err))
        except gidgetlab.exceptions.GitLabBroken as err:
            log.error("[call_api] exception: %s", str(err))
        return res


async def sub_groups():
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        # Make your requests, e.g. ...
        url = "{base_url}api/v4/groups/{gitlab_user}/subgroups?simple=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        subgroups = [g["id"] for g in data]
        return subgroups


async def main_project(project_id, branch):
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        url = (
            "{base_url}api/v4/projects/{project_id}"
            "/pipelines?simple=true&per_page={per_page:d}"
            "&page={page:d}&order_by=id&sort=desc"
            "&ref={branch}".format(
                base_url=GITLAB_URL,
                project_id=project_id,
                per_page=1,
                page=1,
                branch=branch,
            )
        )
        data = await gl.getitem(url)
        return data


# async def main():
#     async with aiohttp.ClientSession() as session:
#         gl = gidgetlab.aiohttp.GitLabAPI(
#             session, GITLAB_USER, access_token=gl_token
#         )
#         url = "{base_url}api/v4/groups/{gitlab_user}/projects?simple=true&license=true&per_page={per_page:d}".format(
#             base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
#         )
#         data = await gl.getitem(url)
#         return data


async def get_project(project):
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        url = "{base_url}api/v4/projects/{project:d}?license=true".format(
            base_url=GITLAB_URL, project=project
        )
        data = await gl.getitem(url)
        return data


async def get_default_branch(project, default):
    try:
        async with aiohttp.ClientSession() as session:
            gl = gidgetlab.aiohttp.GitLabAPI(
                session, GITLAB_USER, access_token=gl_token
            )
            # Make your requests, e.g. ...
            url = "{base_url}api/v4/projects/{project:d}/repository/branches/{default}".format(
                base_url=GITLAB_URL, project=project, default=default
            )
            data = await gl.getitem(url)
        return data
    except (
        gidgetlab.exceptions.BadRequest,
        gidgetlab.exceptions.GitLabBroken,
        aiohttp.ClientOSError,
    ) as err:
        log.debug("[get_default_branch] failed: %s", err)
        return {}


async def fetch(url):
    """Generic HTTP Client"""
    async with aiohttp.ClientSession() as http_connector:
        # await asyncio.sleep(0.10)
        data = await http_connector.get(url)
        if data.status == 200:
            return await data.text()
        return ""


@click.command()
@click.option(
    "--selectedpi",
    envvar="PI",
    default=30,
    show_default=True,
    help="Selected PI",
)
def main(
    selectedpi: int = 30,
):
    # groups_df = pandas.DataFrame()
    projects_df = pandas.DataFrame()
    # mrs_df = pandas.DataFrame()

    # calculate PI boundaries
    selected_pi = selectedpi
    first_pi = 1
    # select_pis = 5 + 1
    this_pi = datetime.strptime("2018-12-12", "%Y-%m-%d").replace(tzinfo=timezone.utc)
    the_pis = []
    last_start = ""
    for pi in range(first_pi, selected_pi + 1):
        end_date = this_pi + timedelta(days=(13 * 7) + 1)
        if pi == 10:
            end_date = end_date - timedelta(days=3)
            this_pi = this_pi - timedelta(days=2)
        if pi == 14:
            this_pi = this_pi - timedelta(days=3)
        if pi == 15:
            end_date = end_date - timedelta(days=5)
            this_pi = this_pi - timedelta(days=4)
        if pi == 16:
            end_date = end_date - timedelta(days=1)
        if pi == 17:
            end_date = end_date - timedelta(days=1)
        if pi == 18:
            end_date = end_date - timedelta(days=1)
        if pi == 19:
            end_date = end_date - timedelta(days=1)
        if pi == 20:
            end_date = end_date - timedelta(days=1)
        if pi == 21:
            end_date = end_date - timedelta(days=1)
        if pi == 22:
            end_date = end_date - timedelta(days=1)
        if pi == 23:
            end_date = end_date - timedelta(days=1)
        if pi == 24:
            end_date = end_date - timedelta(days=1)
        if pi == 25:
            end_date = end_date - timedelta(days=1)
        if pi == 26:
            end_date = end_date - timedelta(days=1)
        if pi == 27:
            end_date = end_date - timedelta(days=1)
        if pi == 28:
            end_date = end_date - timedelta(days=1)
        if pi == 29:
            end_date = end_date - timedelta(days=1)
        if pi == 30:
            end_date = end_date - timedelta(days=1)
        sp_start = this_pi
        sprints = []
        for sp in range(1, 6, 1):
            sp_end = (sp_start + timedelta(days=14)) - timedelta(seconds=1)
            sprints.append(
                {
                    "sprint": sp,
                    "start_time": sp_start,
                    "start": sp_start.strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "finish_time": sp_end,
                    "finish": sp_end.strftime("%Y-%m-%dT%H:%M:%SZ"),
                }
            )
            sp_start = sp_end + timedelta(seconds=1)
        # last sprint goes to the end of PI
        sprints.append(
            {
                "sprint": 6,
                "start_time": sp_start,
                "start": sp_start.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "finish_time": (end_date - timedelta(seconds=1)),
                "finish": (end_date - timedelta(seconds=1)).strftime(
                    "%Y-%m-%dT%H:%M:%SZ"
                ),
            }
        )

        the_pis.append(
            {
                "pi": "PI{}".format(pi),
                "id": pi,
                "start": this_pi.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "started": this_pi.strftime("%Y-%m-%d"),
                "start_time": this_pi,
                "finish": (end_date - timedelta(seconds=1)).strftime(
                    "%Y-%m-%dT%H:%M:%SZ"
                ),
                "finish_time": (end_date - timedelta(seconds=1)),
                "sprints": sprints,
            }
        )
        last_start = this_pi
        this_pi = end_date

    last_start = last_start.strftime("%Y-%m-%dT%H:%M:%SZ")

    pis = the_pis.copy()
    current_pi = pis.pop(-1)
    log.info("Current PI: " + str(current_pi))
    log.info("Last start: " + last_start)

    def which_pi(dt):
        """Which PI is this?"""
        for pi in the_pis:
            if dt >= pi["start_time"] and dt <= pi["finish_time"]:
                for sp in pi["sprints"]:
                    if dt >= sp["start_time"] and dt <= sp["finish_time"]:
                        log.debug(
                            "[1] pi and sprint: %s => %d / %d",
                            dt,
                            pi["id"],
                            sp["sprint"],
                        )
                        return pi["id"]
                log.debug("[2] pi and sprint: %s => %d / %d", dt, pi["id"], 1)
                return pi["id"]
        log.debug("[3] pi and sprint: %s => %d / %d", dt, 1, 1)
        return 1

    def which_sprint(dt):
        """Which PI is this?"""
        for pi in the_pis:
            if dt >= pi["start_time"] and dt <= pi["finish_time"]:
                for sp in pi["sprints"]:
                    if dt >= sp["start_time"] and dt <= sp["finish_time"]:
                        log.debug(
                            "[1] pi and sprint: %s => %d / %d",
                            dt,
                            pi["id"],
                            sp["sprint"],
                        )
                        return sp["sprint"]
                log.debug("[2] pi and sprint: %s => %d / %d", dt, pi["id"], 1)
                return 1
        log.debug("[3] pi and sprint: %s => %d / %d", dt, 1, 1)
        return 1

    # data = asyncio.run(sub_groups())
    # data = asyncio.run(main())  # cluster-k8s
    # data = asyncio.run(main_project(16996947, "master"))  # cluster-k8s
    # data = asyncio.run(main_projects())
    groups = asyncio.run(sub_groups())
    # print(data)
    print("SubGroups: {}".format(len(groups)))
    projects = []
    # 3180705 - ska-telescope group
    groups.append(3180705)
    pkeys = []
    for group in groups:
        print("Group: {}".format(group))
        projs = asyncio.run(sub_group_projects(group))
        for p in projs:
            if not p["id"] in pkeys:
                projects.append(p)
                pkeys.append(p["id"])

    print("Total Projects: {}".format(len(projects)))

    rows = []
    newrows = []
    cnt = 0
    for p in projects:
        proj = asyncio.run(get_project(p["id"]))
        cnt += 1
        # print(proj)
        default_branch = ""
        license_url = ""
        text = ""
        author_email = ""
        author_name = ""
        art = ""
        scan_license = ""
        scan_confidence = ""
        if "default_branch" in p:
            default_branch = p["default_branch"]
            license_url = p["web_url"] + "/-/raw/" + p["default_branch"] + "/LICENSE"
            # if proj['license']:
            # time.sleep(1)
            log.info("[%d] Get license for: %s", cnt, p["path_with_namespace"])
            try:
                res = asyncio.run(fetch(license_url))
            except:
                # sometimes failes first time...
                res = asyncio.run(fetch(license_url))
            text = " ".join(res.split("\n")[0:4])
            match = lookup.match(res)
            # print(match)
            scan_confidence = "0.0"
            scan_license = ""
            if isinstance(match, LicenseMatch):
                scan_confidence = "%.2f" % match.confidence
                match = match.license
                scan_license = "%s" % match.id
                # print("Id: %s" % scan_license)
                # print("Name: %s" % match.name)
                # print("Confidence: %s" % scan_confidence)
                # print("OSI approved: %s" % ("yes" if match.osi_approved is True else "no"))
                # if res.notes:
                #     print("Notes:")
                #     print(indent(wrap(res.notes), '  '))
                # if res.header:
                #     print("Header:")
                #     print(indent(wrap(res.header), '  '))

            res = asyncio.run(get_default_branch(p["id"], default_branch))
            if "commit" in res:
                author_email = res["commit"]["author_email"]
                author_name = res["commit"]["author_name"]
            art = ""
            if int(p["id"]) in PROJECTS:
                art = PROJECTS[int(p["id"])]["ART"]

        key = "Unknown"
        nickname = ""
        name = ""
        if proj["license"]:
            key = proj["license"]["key"]
            nickname = proj["license"]["nickname"]
            name = proj["license"]["name"]
        if "other" in key:
            key = "other"
        group = p["path_with_namespace"].split("/")
        group = group[-2] if len(group) >= 3 else "Main"

        d = [
            p["id"],
            p["path_with_namespace"],
            p["created_at"],
            group,
            art,
            ("Yes" if p["archived"] else "No"),
            default_branch,
            p["web_url"],
            license_url,
            author_email,
            author_name,
            key,
            nickname,
            name,
            text,
            scan_license,
            scan_confidence,
        ]
        # d.extend([key, nickname, name, text])
        if not proj["id"] in EXT_PROJECT_KEYS:
            newrows.append(d)
            # log.error("[getproj] missing project: %s", str(proj))
        else:
            rows.append(d)

    # remove duplicates
    # import json
    rows = sorted(rows, key=lambda x: x[0])
    # rows = list(set([json.dumps(r) for r in rows]))
    # rows = sorted(rows, key=lambda x: x[0])
    # rows = [json.loads(r) for r in rows]

    # output as a csv file
    header = [
        "id",
        "project",
        "created_at",
        "group",
        "art",
        "archived",
        "default_branch",
        "web_url",
        "license_url",
        "author_email",
        "author_name",
        "license_key",
        "shortname",
        "name",
        "short_text",
        "scan_license",
        "scan_confidence",
    ]

    with open("licenses.csv", "w", encoding="UTF8") as f:
        writer = csv.writer(f)
        writer.writerow(header)
        for r in rows:
            writer.writerow(r)
        writer.writerow(header)
        for r in newrows:
            writer.writerow(r)

    projects = []
    project_cnt = 0
    for r in rows:
        dfrow = dict(zip(header, r))
        projects.append(dfrow)
        project_cnt += 1
        if not project_cnt % 1000:
            log.info("projects: accumulated: %d", project_cnt)

    log.info("projects: finished collecting: %d", project_cnt)
    projects_df = pandas.DataFrame.from_records(projects)

    projects_df.set_index("id")
    projects_df["created_at"] = (
        projects_df["created_at"].apply(str).apply(pandas.Timestamp)
    )
    projects_df["pi"] = projects_df["created_at"].apply(which_pi)
    projects_df["sprint"] = projects_df["created_at"].apply(which_sprint)
    log.info(projects_df)
    load_table_dataframe(
        projects_df,
        PROJECT_KEYS,
        "jira-reporting-poc.gitlabmetrics.licenses",
    )
    log.info("projects: finished loading: %d", project_cnt)

    sys.exit(0)


if __name__ == "__main__":
    main()


###############################################################################


# gl = gitlab.Gitlab(GITLAB_URL, private_token=gl_token, api_version=4)

# try:
#     gl.auth()
# except GitlabAuthenticationError:
#     log.error("Authentication error: did you provide a valid token?")
#     sys.exit(1)
# except GitlabGetError as err:
#     response = json.loads(err.response_body.decode())
#     log.error("Problem contacting Gitlab: {}".format(response["message"]))
#     sys.exit(1)

# print("ok")

# project = gl.projects.get(project_name)
# branch = project.branches.get(branch_name)
# top_commit = project.commits.get(branch.commit["short_id"])
# pipelines = project.pipelines.list()


# # print("\n\nProject:")
# # print("\n\nProject[{}]:".format(project_name))
# # print(project)
# # print("\n\nBranch[{}]:".format(branch_name))
# # print(branch)
# # print("\n\nTop Commit:")
# # print(top_commit)

# first = pipelines.pop(0)
# pipeline = project.pipelines.get(first.id)

# # print("\n\nFirst Pipeline: {}".format(first))

# # last pipeline status, coverage
# print("\n\nThe Pipeline: {}".format(pipeline))

# test_report_url = "{}api/v4/projects/{}/pipelines/{}/test_report".format(
#     GITLAB_URL, project.id, pipeline.id
# )
# print("\n\nThe URL: {}".format(test_report_url))
# res = gl.http_get(test_report_url)

# print(dir(res))
# print(type(res))
# print(res)
