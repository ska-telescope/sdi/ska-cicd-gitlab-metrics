#!/usr/bin/env python3

import asyncio
import json
import logging
import os
import pathlib
import pickle
import random
import re
import sys
import time
from datetime import datetime, timedelta, timezone

import aiohttp
import click
import gidgetlab.aiohttp
import pandas
import requests
import spdx_lookup as lookup
from google.cloud import bigquery
from spdx_lookup import LicenseMatch

GITLAB_URL = "https://gitlab.com/"
GITLAB_USER = "ska-telescope"

LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "ERROR").upper()
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] [thread=%(threadName)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("load-bigquery")

rtd_token = ""
if os.environ.get("RTD_TOKEN"):
    rtd_token = os.environ.get("RTD_TOKEN")
rtd_headers = {"Authorization": f"Token {rtd_token}"}
log.info("RTD auth: %s", rtd_headers)

# find Jira issue number in text
jira_search = r"([a-zA-Z][a-zA-Z0-9]+\-[0-9]+).*"
jira_regex = re.compile(jira_search)


gl_token = os.getenv("GITLAB_TOKEN")

STATES = [
    "CREATED",
    "WAITING_FOR_RESOURCE",
    "PREPARING",
    "PENDING",
    "RUNNING",
    "FAILED",
    "SUCCESS",
    "CANCELED",
    "SKIPPED",
    "MANUAL",
    "SCHEDULED",
]

GROUP_KEYS = {
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "gid": bigquery.enums.SqlTypeNames.STRING,
    "name": bigquery.enums.SqlTypeNames.STRING,
    "path": bigquery.enums.SqlTypeNames.STRING,
    "fullPath": bigquery.enums.SqlTypeNames.STRING,
}

groups_graphql_query = """
    query ($cursor: String) {
        groups(search: "ska-telescope", first: 100, after: $cursor) {
            pageInfo {
                endCursor
                hasNextPage
            }
            nodes {
                name
                path
                fullPath
                id
            }
        }
    }
"""

PROJECT_KEYS = {
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "gid": bigquery.enums.SqlTypeNames.STRING,
    "name": bigquery.enums.SqlTypeNames.STRING,
    "path": bigquery.enums.SqlTypeNames.STRING,
    "fullPath": bigquery.enums.SqlTypeNames.STRING,
    "createdAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "archived": bigquery.enums.SqlTypeNames.BOOLEAN,
    "description": bigquery.enums.SqlTypeNames.STRING,
    "nameWithNamespace": bigquery.enums.SqlTypeNames.STRING,
    "visibility": bigquery.enums.SqlTypeNames.STRING,
    "empty": bigquery.enums.SqlTypeNames.BOOLEAN,
    "exists": bigquery.enums.SqlTypeNames.BOOLEAN,
    "default_branch": bigquery.enums.SqlTypeNames.STRING,
    "group_id": bigquery.enums.SqlTypeNames.INTEGER,
    "group_gid": bigquery.enums.SqlTypeNames.STRING,
    "group_name": bigquery.enums.SqlTypeNames.STRING,
    "group_fullName": bigquery.enums.SqlTypeNames.STRING,
    "group_path": bigquery.enums.SqlTypeNames.STRING,
    "group_fullPath": bigquery.enums.SqlTypeNames.STRING,
    "pi": bigquery.enums.SqlTypeNames.INTEGER,
    "sprint": bigquery.enums.SqlTypeNames.INTEGER,
    "coverage_averageCoverage": bigquery.enums.SqlTypeNames.FLOAT,
    "coverage_coverageCount": bigquery.enums.SqlTypeNames.INTEGER,
    "coverage_lastUpdatedOn": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "vulnerability_critical": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_high": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_info": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_low": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_medium": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_unknown": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_scanners": bigquery.enums.SqlTypeNames.STRING,
    "group": bigquery.enums.SqlTypeNames.STRING,
    "author_email": bigquery.enums.SqlTypeNames.STRING,
    "author_name": bigquery.enums.SqlTypeNames.STRING,
    "scan_license": bigquery.enums.SqlTypeNames.STRING,
    "scan_confidence": bigquery.enums.SqlTypeNames.STRING,
    "got_tests": bigquery.enums.SqlTypeNames.BOOLEAN,
    "got_linting": bigquery.enums.SqlTypeNames.BOOLEAN,
    "got_coverage": bigquery.enums.SqlTypeNames.BOOLEAN,
    "got_rtd": bigquery.enums.SqlTypeNames.BOOLEAN,
}

projects_graphql_query = """
    query ($cursor: String) {
        group(fullPath: "ska-telescope") {
            name
            path
            projects(includeSubgroups: true, first: 100, after: $cursor) {
                pageInfo {
                    endCursor
                    hasNextPage
                }
                edges {
                    node {
                        id
                        fullPath
                        path
                        createdAt
                        archived
                        description
                        name
                        webUrl
                        nameWithNamespace
                        visibility
                        repository {
                            empty
                            exists
                            rootRef
                        }
                        group {
                            id
                            name
                            fullName
                            path
                            fullPath
                        }
                        vulnerabilitySeveritiesCount{
                            critical
                            high
                            info
                            low
                            medium
                            unknown
                        }
                        vulnerabilityScanners{
                            nodes {
                                name
                            }
                        }

                        codeCoverageSummary {
                          averageCoverage
                          coverageCount
                          lastUpdatedOn
                        }
                    }
                }
            }
        }
    }

"""


VULNERABILITIES_KEYS = {
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "gid": bigquery.enums.SqlTypeNames.STRING,
    "name": bigquery.enums.SqlTypeNames.STRING,
    "path": bigquery.enums.SqlTypeNames.STRING,
    "fullPath": bigquery.enums.SqlTypeNames.STRING,
    "createdAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "archived": bigquery.enums.SqlTypeNames.BOOLEAN,
    "description": bigquery.enums.SqlTypeNames.STRING,
    "nameWithNamespace": bigquery.enums.SqlTypeNames.STRING,
    "visibility": bigquery.enums.SqlTypeNames.STRING,
    "empty": bigquery.enums.SqlTypeNames.BOOLEAN,
    "exists": bigquery.enums.SqlTypeNames.BOOLEAN,
    "default_branch": bigquery.enums.SqlTypeNames.STRING,
    "pi": bigquery.enums.SqlTypeNames.INTEGER,
    "sprint": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_critical": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_high": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_info": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_low": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_medium": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_unknown": bigquery.enums.SqlTypeNames.INTEGER,
    "vulnerability_scanners": bigquery.enums.SqlTypeNames.STRING,
}

vulnerabilities_graphql_query = """
    query ($cursor: String) {
        group(fullPath: "ska-telescope") {
            name
            path
            projects(includeSubgroups: true) {
                edges {
                    node {
                        id
                        fullPath
                        path
                        createdAt
                        archived
                        description
                        name
                        nameWithNamespace
                        visibility
                        vulnerabilities (first: 100, after: $cursor) {
                            pageInfo {
                                endCursor
                                hasNextPage
                            }

                            edges {
                                node {
                                    presentOnDefaultBranch
                                    dismissedAt
                                    dismissalReason
                                    hasRemediations
                                    detectedAt
                                    severity
                                    state
                                    title
                                    resolvedAt
                                    resolvedBy {
                                        name
                                    }
                                    identifiers {
                                        externalId
                                        externalType
                                        name
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

"""


MR_KEYS = {
    "iid": bigquery.enums.SqlTypeNames.INTEGER,
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "gid": bigquery.enums.SqlTypeNames.STRING,
    "empty": bigquery.enums.SqlTypeNames.BOOLEAN,
    "exists": bigquery.enums.SqlTypeNames.BOOLEAN,
    "createdAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "updatedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "mergedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "description": bigquery.enums.SqlTypeNames.STRING,
    "title": bigquery.enums.SqlTypeNames.STRING,
    "state": bigquery.enums.SqlTypeNames.STRING,
    "jira_id": bigquery.enums.SqlTypeNames.STRING,
    "targetBranch": bigquery.enums.SqlTypeNames.STRING,
    "default_branch": bigquery.enums.SqlTypeNames.STRING,
    "projectId": bigquery.enums.SqlTypeNames.INTEGER,
    "project_id": bigquery.enums.SqlTypeNames.INTEGER,
    "project_gid": bigquery.enums.SqlTypeNames.STRING,
    "project_name": bigquery.enums.SqlTypeNames.STRING,
    "project_nameWithNamespace": bigquery.enums.SqlTypeNames.STRING,
    "project_path": bigquery.enums.SqlTypeNames.STRING,
    "project_createdAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "project_archived": bigquery.enums.SqlTypeNames.BOOLEAN,
    "project_description": bigquery.enums.SqlTypeNames.STRING,
    "project_visibility": bigquery.enums.SqlTypeNames.STRING,
    "author_id": bigquery.enums.SqlTypeNames.INTEGER,
    "author_gid": bigquery.enums.SqlTypeNames.STRING,
    "author_name": bigquery.enums.SqlTypeNames.STRING,
    "author_username": bigquery.enums.SqlTypeNames.STRING,
    "mergeUser_id": bigquery.enums.SqlTypeNames.INTEGER,
    "mergeUser_gid": bigquery.enums.SqlTypeNames.STRING,
    "mergeUser_name": bigquery.enums.SqlTypeNames.STRING,
    "additions": bigquery.enums.SqlTypeNames.INTEGER,
    "changes": bigquery.enums.SqlTypeNames.INTEGER,
    "deletions": bigquery.enums.SqlTypeNames.INTEGER,
    # "pipeline_count": bigquery.enums.SqlTypeNames.INTEGER,
    # "latest_test_count": bigquery.enums.SqlTypeNames.INTEGER,
    # "latest_test_skipped": bigquery.enums.SqlTypeNames.INTEGER,
    # "latest_test_success": bigquery.enums.SqlTypeNames.INTEGER,
    # "latest_test_error": bigquery.enums.SqlTypeNames.INTEGER,
    # "latest_test_failed": bigquery.enums.SqlTypeNames.INTEGER,
    # "latest_test_time": bigquery.enums.SqlTypeNames.INTEGER,
    # "latest_test_suite_msg": bigquery.enums.SqlTypeNames.STRING,
    # "latest_pipeline_status": bigquery.enums.SqlTypeNames.STRING,
    # "latest_pipeline_duration": bigquery.enums.SqlTypeNames.FLOAT,
    # "latest_pipeline_coverage": bigquery.enums.SqlTypeNames.FLOAT,
    # "latest_pipeline_startedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    # "latest_pipeline_finishedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    # "latest_pipeline_updatedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    # "latest_pipeline_ref": bigquery.enums.SqlTypeNames.STRING,
    "pi": bigquery.enums.SqlTypeNames.INTEGER,
    "sprint": bigquery.enums.SqlTypeNames.INTEGER,
}
# for state in STATES:
#     MR_KEYS["state_" + state.lower()] = bigquery.enums.SqlTypeNames.INTEGER

mrs_graphql_query = """
    query ($cursor: String) {
        project(fullPath: "{project_fullpath}") {
                    id
                    fullPath
                    path
                    createdAt
                    archived
                    description
                    name
                    nameWithNamespace
                    visibility
                    repository {
                        empty
                        exists
                        rootRef
                    }
                    mergeRequests(
                        sort: CREATED_ASC
                        createdAfter: "{created_after}"
                        createdBefore: "{created_before}"
                        after: $cursor
                        ) {
                            pageInfo {
                                endCursor
                                hasNextPage
                            }
                            edges {
                                node {
                                    id
                                    iid
                                    sourceBranch
                                    targetBranch
                                    createdAt
                                    updatedAt
                                    mergedAt
                                    approved
                                    state
                                    title
                                    description
                                    projectId
                                    mergeUser {
                                        id
                                        name
                                        username
                                    }
                                    author {
                                        id
                                        name
                                        username
                                    }
                                    diffStatsSummary {
                                        additions
                                        changes
                                        deletions
                                    }
                                }
                            }
                        }
            }
        }
"""


def template_mrs_query(template, project_fullpath, created_after, created_before):
    """template values for the MR query"""
    return (
        template.replace("{project_fullpath}", project_fullpath)
        .replace("{created_after}", created_after)
        .replace("{created_before}", created_before)
    )


PIPELINE_KEYS = {
    "iid": bigquery.enums.SqlTypeNames.INTEGER,
    "id": bigquery.enums.SqlTypeNames.INTEGER,
    "gid": bigquery.enums.SqlTypeNames.STRING,
    "empty": bigquery.enums.SqlTypeNames.BOOLEAN,
    "exists": bigquery.enums.SqlTypeNames.BOOLEAN,
    # "status": bigquery.enums.SqlTypeNames.STRING,
    # "duration": bigquery.enums.SqlTypeNames.FLOAT,
    # "coverage": bigquery.enums.SqlTypeNames.FLOAT,
    # "startedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    # "finishedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    # "updatedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    # "ref": bigquery.enums.SqlTypeNames.STRING,
    # "projectId": bigquery.enums.SqlTypeNames.INTEGER,
    "project_id": bigquery.enums.SqlTypeNames.INTEGER,
    "project_gid": bigquery.enums.SqlTypeNames.STRING,
    "project_name": bigquery.enums.SqlTypeNames.STRING,
    "project_nameWithNamespace": bigquery.enums.SqlTypeNames.STRING,
    "project_path": bigquery.enums.SqlTypeNames.STRING,
    "project_createdAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "project_archived": bigquery.enums.SqlTypeNames.BOOLEAN,
    "project_description": bigquery.enums.SqlTypeNames.STRING,
    "project_visibility": bigquery.enums.SqlTypeNames.STRING,
    # MR
    "mr_id": bigquery.enums.SqlTypeNames.INTEGER,
    "createdAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "updatedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "mergedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "description": bigquery.enums.SqlTypeNames.STRING,
    "title": bigquery.enums.SqlTypeNames.STRING,
    "jira_id": bigquery.enums.SqlTypeNames.STRING,
    "sourceBranch": bigquery.enums.SqlTypeNames.STRING,
    "targetBranch": bigquery.enums.SqlTypeNames.STRING,
    "default_branch": bigquery.enums.SqlTypeNames.STRING,
    "state": bigquery.enums.SqlTypeNames.STRING,
    "approved": bigquery.enums.SqlTypeNames.BOOLEAN,
    # "author_id": bigquery.enums.SqlTypeNames.INTEGER,
    # "author_gid": bigquery.enums.SqlTypeNames.STRING,
    # "author_name": bigquery.enums.SqlTypeNames.STRING,
    # "author_username": bigquery.enums.SqlTypeNames.STRING,
    # "mergeUser_id": bigquery.enums.SqlTypeNames.INTEGER,
    # "mergeUser_gid": bigquery.enums.SqlTypeNames.STRING,
    # "mergeUser_name": bigquery.enums.SqlTypeNames.STRING,
    # "additions": bigquery.enums.SqlTypeNames.INTEGER,
    # "changes": bigquery.enums.SqlTypeNames.INTEGER,
    # "deletions": bigquery.enums.SqlTypeNames.INTEGER,
    # "pipeline_count": bigquery.enums.SqlTypeNames.INTEGER,
    "test_skipped": bigquery.enums.SqlTypeNames.INTEGER,
    "test_success": bigquery.enums.SqlTypeNames.INTEGER,
    "test_error": bigquery.enums.SqlTypeNames.INTEGER,
    "test_failed": bigquery.enums.SqlTypeNames.INTEGER,
    "test_time": bigquery.enums.SqlTypeNames.INTEGER,
    "test_suite_msg": bigquery.enums.SqlTypeNames.STRING,
    "pipeline_status": bigquery.enums.SqlTypeNames.STRING,
    "pipeline_duration": bigquery.enums.SqlTypeNames.FLOAT,
    "pipeline_coverage": bigquery.enums.SqlTypeNames.FLOAT,
    "pipeline_startedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "pipeline_finishedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "pipeline_updatedAt": bigquery.enums.SqlTypeNames.TIMESTAMP,
    "pipeline_ref": bigquery.enums.SqlTypeNames.STRING,
    "pi": bigquery.enums.SqlTypeNames.INTEGER,
    "sprint": bigquery.enums.SqlTypeNames.INTEGER,
}

pipelines_graphql_query = """
     query ($cursor: String) {
        project(fullPath: "{project_fullpath}") {
            id
            fullPath
            path
            createdAt
            archived
            description
            name
            nameWithNamespace
            visibility
            repository {
                empty
                exists
                rootRef
            }
            pipelines(
                first: 100
                updatedAfter: "{updated_after}"
                updatedBefore: "{updated_before}"
                after: $cursor
            ) {
                pageInfo {
                    endCursor
                    hasNextPage
                }
                edges {
                    node {
                        id
                        iid
                        status
                        duration
                        coverage
                        startedAt
                        finishedAt
                        updatedAt
                        ref
                        testReportSummary {
                            total {
                                count
                                skipped
                                success
                                error
                                failed
                                time
                                suiteError
                            }
                        }
                        mergeRequest {
                            id
                            sourceBranch
                            targetBranch
                            createdAt
                            updatedAt
                            mergedAt
                            approved
                            state
                            title
                            description
                        }
                    }
                }
            }
        }
    }
"""


def template_pipelines_query(template, project_fullpath, updated_after, updated_before):
    """template values for the MR query"""
    return (
        template.replace("{project_fullpath}", project_fullpath)
        .replace("{updated_after}", updated_after)
        .replace("{updated_before}", updated_before)
    )


def fetch_graphql_data(query, variables):
    # Your code to fetch GraphQL data here, using the requests library
    # Make sure to replace this with your actual GraphQL fetching mechanism
    url = "https://gitlab.com/api/graphql"
    headers = {
        "Content-Type": "application/json",
        "Authorization": gl_token,
    }
    log.debug("GQL request headers: %s", headers)
    log.debug("GQL request variables: %s", variables)
    log.debug("GQL request query: %s", query)
    cnt = 0
    response = None
    while True:
        cnt += 1
        response = requests.post(
            url, json={"query": query, "variables": variables}, headers=headers
        )
        if response.status_code == 200:
            break
        if cnt > 15:
            log.critical("Failed too many times - aborting")
            sys.exit(1)
        log.error("request failed - retrying after %d secs: %s", cnt, response)
        time.sleep(cnt)

    log.debug("GQL response: %s", response)
    data = response.json()
    return data


def load_table_dataframe(
    table_df: pandas.DataFrame, table_keys: dict, table_id: str
) -> "bigquery.Table":
    """dynamically load a dataframe into big query"""

    # Construct a BigQuery client object.
    client = bigquery.Client()

    for k in table_keys.keys():
        log.debug("table: %s key: %s", table_id, k)
        log.debug("column: %s", table_df[k])
        if table_keys[k] == bigquery.enums.SqlTypeNames.TIMESTAMP:
            # table_df[k].fillna("1970-01-01 00:00:00")
            table_df[k] = table_df[k].apply(
                lambda x: (
                    "1970-01-01 00:00:00"
                    if (
                        (isinstance(x, str) and (x.isspace() or x == "None" or not x))
                        or x is None
                    )
                    else x
                )
            )
            table_df[k] = table_df[k].apply(str).apply(pandas.Timestamp)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.INTEGER:
            table_df[k] = table_df[k].astype(int)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.BOOLEAN:
            table_df[k] = table_df[k].astype(bool)
        elif table_keys[k] == bigquery.enums.SqlTypeNames.FLOAT:
            table_df[k] = table_df[k].astype(float)
        else:
            table_df[k] = table_df[k].astype(str)

    df_schema = []
    for col in table_df.columns:
        if col in table_keys:
            df_schema.append(bigquery.SchemaField(col, table_keys[col]))

    job_config = bigquery.LoadJobConfig(
        # Specify a (partial) schema. All columns are always written to the
        # table. The schema is used to assist in data type definitions.
        schema=df_schema,
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        # alternately - WRITE_APPEND
        write_disposition="WRITE_TRUNCATE",
    )

    job = client.load_table_from_dataframe(
        table_df[table_keys.keys()], table_id, job_config=job_config
    )  # Make an API request.
    job.result()  # Wait for the job to complete.

    table = client.get_table(table_id)  # Make an API request.
    log.info(
        "Loaded {} rows and {} columns to {}".format(
            table.num_rows, len(table.schema), table_id
        )
    )
    # [END bigquery_load_table_dataframe]
    return table


async def get_default_branch(project, default):
    # get the default branch of the project from the classic API
    try:
        async with aiohttp.ClientSession() as session:
            gl = gidgetlab.aiohttp.GitLabAPI(
                session, GITLAB_USER, access_token=gl_token
            )
            # Make your requests, e.g. ...
            url = "{base_url}api/v4/projects/{project:d}/repository/branches/{default}".format(
                base_url=GITLAB_URL, project=project, default=default
            )
            data = await gl.getitem(url)
        return data
    except (
        gidgetlab.exceptions.BadRequest,
        gidgetlab.exceptions.GitLabBroken,
        aiohttp.ClientOSError,
    ) as err:
        log.debug("[get_default_branch] failed: %s", err)
        return {}


async def get_badges(project):
    # get the badges of the project from the classic API
    try:
        async with aiohttp.ClientSession() as session:
            gl = gidgetlab.aiohttp.GitLabAPI(
                session, GITLAB_USER, access_token=gl_token
            )
            # Make your requests, e.g. ...
            url = "{base_url}api/v4/projects/{project:d}/badges".format(
                base_url=GITLAB_URL, project=project
            )
            data = await gl.getitem(url)
        return data
    except (
        gidgetlab.exceptions.BadRequest,
        gidgetlab.exceptions.GitLabBroken,
        aiohttp.ClientOSError,
    ) as err:
        log.debug("[get_badges] failed: %s", err)
        return {}


def fetchRTD(url):
    """Generic HTTP Client"""
    import requests
    data = requests.get(url, headers=rtd_headers)
    log.debug("fetch RTD %s: %s", url, data)
    if data.status_code == 200:
        return True
    return False


async def fetch(url):
    """Generic HTTP Client"""
    async with aiohttp.ClientSession() as http_connector:
        # await asyncio.sleep(0.10)
        data = await http_connector.get(url)
        if data.status == 200:
            return await data.text()
        return ""


async def find_badge_item(item_name, items, project):
    """find whether the badge image actually exists for a metric"""
    ppath = project["fullPath"] if project["fullPath"] else ""
    branch = project["default_branch"] if project["default_branch"] else ""
    for item in items:
        if item["name"] == item_name:
            # now check the URL actually serves
            # https://artefact.skao.int/repository/raw-internal/gitlab-ci-metrics/ska-telescope/sdi/ska-ci-cd-deployment-on-stfc-cloud/master/badges/lint_total.svg
            url = (
                item["image_url"]
                .replace("%{project_path}", ppath)
                .replace("%{default_branch}", branch)
            )
            try:
                res = await fetch(url)
                log.debug("fetch image %s: %s [%s]", item_name, url, res)
                return res  # not empty
            except aiohttp.client_exceptions.ServerDisconnectedError as ex:
                log.error("fetch image failed! %s: %s [%s]", item_name, url, ex)
                return False
    return False


@click.command()
@click.option(
    "--datasets",
    envvar="BEFORE",
    default="datasets",
    show_default=True,
    help="Directory to dump/load datasets",
)
@click.option(
    "--before",
    envvar="BEFORE",
    default=str(datetime.now().strftime("%Y-%m-%d")),
    show_default=True,
    help="MRs before date",
)
@click.option(
    "--after",
    envvar="AFTER",
    default="2019-01-01",
    show_default=True,
    help="MRs after date",
)
@click.option(
    "--skipgroups",
    envvar="SKIP_GROUPS",
    is_flag=True,
    default=False,
    help="Skip processing groups",
)
@click.option(
    "--skipprojects",
    envvar="SKIP_PROJECTS",
    is_flag=True,
    default=False,
    help="Skip processing projects",
)
@click.option(
    "--skipmrs",
    envvar="SKIP_MRS",
    is_flag=True,
    default=False,
    help="Skip processing mrs",
)
@click.option(
    "--skippipelines",
    envvar="SKIP_PIPELINES",
    is_flag=True,
    default=False,
    help="Skip processing pipelines",
)
@click.option(
    "--skipupdate",
    envvar="SKIP_UPDATE",
    is_flag=True,
    default=False,
    help="Skip DB update",
)
@click.option(
    "--dump",
    envvar="DUMP",
    is_flag=True,
    default=False,
    help="Dump query results",
)
@click.option(
    "--load",
    envvar="LOAD",
    is_flag=True,
    default=False,
    help="LOAD JSON file query results",
)
@click.option(
    "--selectedpi",
    envvar="PI",
    default=30,  # this needs expanding in future #####
    show_default=True,
    help="Selected PI",
)
def main(
    datasets: str = "datasets",
    after: str = "",
    before: str = "",
    skipgroups: bool = False,
    skipprojects: bool = False,
    skipmrs: bool = False,
    skippipelines: bool = False,
    skipupdate: bool = False,
    dump: bool = False,
    load: bool = False,
    selectedpi: int = 30,
):
    DATASETS_DIR = datasets

    # sort out the projects cache
    project_state = {}
    project_state_file = DATASETS_DIR + "/projects.pickle"
    if os.path.isfile(project_state_file):
        with open(project_state_file, "rb") as fle:
            project_state = pickle.load(fle)

    groups_df = pandas.DataFrame()
    projects_df = pandas.DataFrame()
    mrs_df = pandas.DataFrame()
    all_projects = []

    # calculate PI boundaries
    selected_pi = selectedpi
    first_pi = 1
    # select_pis = 5 + 1
    this_pi = datetime.strptime("2018-12-12", "%Y-%m-%d").replace(tzinfo=timezone.utc)
    the_pis = []
    last_start = ""
    for pi in range(first_pi, selected_pi + 1):
        end_date = this_pi + timedelta(days=(13 * 7) + 1)
        if pi == 10:
            end_date = end_date - timedelta(days=3)
            this_pi = this_pi - timedelta(days=2)
        if pi == 14:
            this_pi = this_pi - timedelta(days=3)
        if pi == 15:
            end_date = end_date - timedelta(days=5)
            this_pi = this_pi - timedelta(days=4)
        if pi == 16:
            end_date = end_date - timedelta(days=1)
        if pi == 17:
            end_date = end_date - timedelta(days=1)
        if pi == 18:
            end_date = end_date - timedelta(days=1)
        if pi == 19:
            end_date = end_date - timedelta(days=1)
        if pi == 20:
            end_date = end_date - timedelta(days=1)
        if pi == 21:
            end_date = end_date - timedelta(days=1)
        if pi == 22:
            end_date = end_date - timedelta(days=1)
        if pi == 23:
            end_date = end_date - timedelta(days=1)
        if pi == 24:
            end_date = end_date - timedelta(days=1)
        if pi == 25:
            end_date = end_date - timedelta(days=1)
        if pi == 26:
            end_date = end_date - timedelta(days=1)
        if pi == 27:
            end_date = end_date - timedelta(days=1)
        if pi == 28:
            end_date = end_date - timedelta(days=1)
        if pi == 29:
            end_date = end_date - timedelta(days=1)
        if pi == 30:
            end_date = end_date - timedelta(days=1)
        sp_start = this_pi
        sprints = []
        for sp in range(1, 6, 1):
            sp_end = (sp_start + timedelta(days=14)) - timedelta(seconds=1)
            sprints.append(
                {
                    "sprint": sp,
                    "start_time": sp_start,
                    "start": sp_start.strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "finish_time": sp_end,
                    "finish": sp_end.strftime("%Y-%m-%dT%H:%M:%SZ"),
                }
            )
            sp_start = sp_end + timedelta(seconds=1)
        # last sprint goes to the end of PI
        sprints.append(
            {
                "sprint": 6,
                "start_time": sp_start,
                "start": sp_start.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "finish_time": (end_date - timedelta(seconds=1)),
                "finish": (end_date - timedelta(seconds=1)).strftime(
                    "%Y-%m-%dT%H:%M:%SZ"
                ),
            }
        )

        the_pis.append(
            {
                "pi": "PI{}".format(pi),
                "id": pi,
                "start": this_pi.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "started": this_pi.strftime("%Y-%m-%d"),
                "start_time": this_pi,
                "finish": (end_date - timedelta(seconds=1)).strftime(
                    "%Y-%m-%dT%H:%M:%SZ"
                ),
                "finish_time": (end_date - timedelta(seconds=1)),
                "sprints": sprints,
            }
        )
        last_start = this_pi
        this_pi = end_date

    last_start = last_start.strftime("%Y-%m-%dT%H:%M:%SZ")

    pis = the_pis.copy()
    current_pi = pis.pop(-1)
    log.info("Current PI: " + str(current_pi))
    log.info("Last start: " + last_start)

    def save_project_state(project_state):
        """Pickle the project state to cache"""
        # now update project history bookmark
        with open(project_state_file, "wb") as fle:
            pickle.dump(project_state, fle)
            fle.flush()

    def dump_query_results(dataset: str, data: list):
        """Dump query results to JSON file"""
        current_datetime = str(datetime.now().strftime("%Y-%m-%d-%H-%M-%S"))
        isExist = os.path.exists(DATASETS_DIR)
        if not isExist:
            os.makedirs(DATASETS_DIR)
        filename = "%s/%s-%s.json" % (DATASETS_DIR, dataset, current_datetime)
        with open(filename, "w", encoding="utf-8") as f:
            json.dump(data, f)
        log.info("Dumped dataset to: %s", filename)

    def which_pi(dt):
        """Which PI is this?"""
        for pi in the_pis:
            if dt >= pi["start_time"] and dt <= pi["finish_time"]:
                for sp in pi["sprints"]:
                    if dt >= sp["start_time"] and dt <= sp["finish_time"]:
                        log.debug(
                            "[1] pi and sprint: %s => %d / %d",
                            dt,
                            pi["id"],
                            sp["sprint"],
                        )
                        return pi["id"]
                log.debug("[2] pi and sprint: %s => %d / %d", dt, pi["id"], 1)
                return pi["id"]
        log.debug("[3] pi and sprint: %s => %d / %d", dt, 1, 1)
        return 1

    def which_sprint(dt):
        """Which PI is this?"""
        for pi in the_pis:
            if dt >= pi["start_time"] and dt <= pi["finish_time"]:
                for sp in pi["sprints"]:
                    if dt >= sp["start_time"] and dt <= sp["finish_time"]:
                        log.debug(
                            "[1] pi and sprint: %s => %d / %d",
                            dt,
                            pi["id"],
                            sp["sprint"],
                        )
                        return sp["sprint"]
                log.debug("[2] pi and sprint: %s => %d / %d", dt, pi["id"], 1)
                return 1
        log.debug("[3] pi and sprint: %s => %d / %d", dt, 1, 1)
        return 1

    # Groups
    if not skipgroups:
        groups = []
        group_cnt = 0
        all_groups = []
        log.info("Querying groups")

        variables = {"cursor": None}  # Initially, no cursor

        try:
            while True:
                response = fetch_graphql_data(groups_graphql_query, variables)
                groups = response["data"]["groups"]["nodes"]
                page_info = response["data"]["groups"]["pageInfo"]
                all_groups.extend(groups)

                if page_info["hasNextPage"]:
                    variables["cursor"] = page_info["endCursor"]
                else:
                    break
        except Exception as e:
            print("An error occurred:", e)
            sys.exit(1)

        if dump:
            dump_query_results("groups", all_groups)

        log.info("Processing groups: %d", len(all_groups))
        for group in all_groups:
            group["gid"] = group["id"]
            # "id": "gid://gitlab/Group/66522311"
            group["id"] = int(group["id"].split("/")[-1])
            dfrow = {k: group[k] for k in GROUP_KEYS.keys() if k in group}
            groups.append(dfrow)
            group_cnt += 1
            if not group_cnt % 1000:
                log.info("groups: accumulated: %d", group_cnt)

        log.info("groups: finished collecting: %d", group_cnt)
        groups_df = pandas.DataFrame.from_records(groups)

        # groups_df["time"] = groups_df["time"].apply(str).apply(pandas.Timestamp)
        # groups_df["pi"] = groups_df["time"].apply(which_pi)
        # groups_df["sprint"] = groups_df["time"].apply(which_sprint)

        groups_df.set_index("gid")
        log.info(groups_df)
        if not skipupdate:
            load_table_dataframe(
                groups_df,
                GROUP_KEYS,
                "jira-reporting-poc.gitlab.gqlgroups",
            )
            log.info("groups: finished loading: %d", group_cnt)
        else:
            log.info("groups: update skipped")

    # Projects
    if not skipprojects:
        projects = []
        project_cnt = 0
        log.info("Querying projects")

        variables = {"cursor": None}  # Initially, no cursor

        try:
            while True:
                # test https://gitlab.com/-/graphql-explorer
                response = fetch_graphql_data(projects_graphql_query, variables)
                projects = [
                    node["node"]
                    for node in response["data"]["group"]["projects"]["edges"]
                ]
                page_info = response["data"]["group"]["projects"]["pageInfo"]
                all_projects.extend(projects)

                if page_info["hasNextPage"]:
                    variables["cursor"] = page_info["endCursor"]
                else:
                    break
        except Exception as e:
            print("An error occurred:", e)
            sys.exit(1)

        if dump:
            dump_query_results("projects", all_projects)

        log.info("Processing projects: %d", len(all_projects))
        for project in all_projects:
            log.debug("project: %s", str(project))
            project["gid"] = project["id"]
            # "id": "gid://gitlab/project/66522311"
            project["id"] = int(project["id"].split("/")[-1])
            project["empty"] = (
                project["repository"]["empty"] if project["repository"] else False
            )
            project["exists"] = (
                project["repository"]["exists"] if project["repository"] else False
            )
            project["default_branch"] = (
                project["repository"]["rootRef"] if project["repository"] else "Unknown"
            )
            project["group_gid"] = project["group"]["id"]
            project["group_id"] = int(project["group"]["id"].split("/")[-1])
            project["group_name"] = project["group"]["name"]
            project["group_fullName"] = project["group"]["fullName"]
            project["group_path"] = project["group"]["path"]
            project["group_fullPath"] = project["group"]["fullPath"]
            project["coverage_averageCoverage"] = (
                project["codeCoverageSummary"]["averageCoverage"]
                if project["codeCoverageSummary"]
                else 0
            )
            project["coverage_coverageCount"] = (
                project["codeCoverageSummary"]["coverageCount"]
                if project["codeCoverageSummary"]
                else 0
            )
            project["coverage_lastUpdatedOn"] = (
                project["codeCoverageSummary"]["lastUpdatedOn"]
                if project["codeCoverageSummary"]
                else None
            )
            project["vulnerability_critical"] = (
                project["vulnerabilitySeveritiesCount"]["critical"]
                if project["vulnerabilitySeveritiesCount"]
                else 0
            )
            project["vulnerability_high"] = (
                project["vulnerabilitySeveritiesCount"]["high"]
                if project["vulnerabilitySeveritiesCount"]
                else 0
            )
            project["vulnerability_info"] = (
                project["vulnerabilitySeveritiesCount"]["info"]
                if project["vulnerabilitySeveritiesCount"]
                else 0
            )
            project["vulnerability_low"] = (
                project["vulnerabilitySeveritiesCount"]["low"]
                if project["vulnerabilitySeveritiesCount"]
                else 0
            )
            project["vulnerability_medium"] = (
                project["vulnerabilitySeveritiesCount"]["medium"]
                if project["vulnerabilitySeveritiesCount"]
                else 0
            )
            project["vulnerability_unknown"] = (
                project["vulnerabilitySeveritiesCount"]["unknown"]
                if project["vulnerabilitySeveritiesCount"]
                else 0
            )
            project["vulnerability_scanners"] = (
                ":".join(project["vulnerabilityScanners"]["nodes"])
                if project["vulnerabilityScanners"]
                else ""
            )

            # take from the cache if we have it
            saved_project = None
            project["cache_date"] = datetime.now()
            # if project["id"] in project_state:
            #     cached_project = project_state[project["id"]]
            #     if "cache_date" in cached_project:
            #         cache_date = cached_project["cache_date"]
            #         now = datetime.now()
            #         diff = (now - cache_date).total_seconds()
            #         log.debug(
            #             "Project cache [date: %s] diff is: %d (%s - %s)",
            #             cache_date,
            #             diff,
            #             now,
            #             cache_date,
            #         )
            #         # randomly select days in seconds between 2 and 5
            #         if diff < (60 * 60 * 24 * (random.randint(2, 5))):
            #             saved_project = cached_project
            #             project["cache_date"] = cached_project["cache_date"]

            # add license processing
            default_branch = ""
            author_email = ""
            author_name = ""
            scan_license = ""
            scan_confidence = ""

            if "default_branch" in project:
                default_branch = project["default_branch"]
                if saved_project:
                    scan_license = saved_project["scan_license"]
                    scan_confidence = saved_project["scan_confidence"]
                    author_email = (
                        saved_project["author_email"]
                        if "author_email" in saved_project
                        else ""
                    )
                    author_name = (
                        saved_project["author_name"]
                        if "author_name" in saved_project
                        else ""
                    )
                else:
                    license_url = (
                        project["webUrl"]
                        + "/-/raw/"
                        + project["default_branch"]
                        + "/LICENSE"
                    )
                    log.debug("Get license for: %s", project["nameWithNamespace"])
                    try:
                        res = asyncio.run(fetch(license_url))
                    except:
                        # sometimes failes first time...
                        res = asyncio.run(fetch(license_url))
                    match = lookup.match(res)
                    scan_confidence = "0.0"
                    scan_license = ""
                    if isinstance(match, LicenseMatch):
                        scan_confidence = "%.2f" % match.confidence
                        match = match.license
                        scan_license = "%s" % match.id
                    res = asyncio.run(get_default_branch(project["id"], default_branch))
                    if "commit" in res:
                        author_email = res["commit"]["author_email"]
                        author_name = res["commit"]["author_name"]

            group = project["nameWithNamespace"].split("/")
            group = group[-2] if len(group) >= 3 else "Main"
            project["group"] = group
            project["author_email"] = author_email
            project["author_name"] = author_name
            project["scan_license"] = scan_license
            project["scan_confidence"] = scan_confidence

            log.debug("Project NOW: %s", project)

            # project level badges
            project["got_tests"] = False
            project["got_linting"] = False
            project["got_coverage"] = False
            project["got_rtd"] = False

            # check the cache
            if saved_project:
                project["got_tests"] = saved_project["got_tests"]
                project["got_linting"] = saved_project["got_linting"]
                project["got_coverage"] = saved_project["got_coverage"]
                project["got_rtd"] = saved_project["got_rtd"]
            else:
                try:
                    # Get the bages from the stdout file generated during
                    # test run
                    project_id = project["id"]
                    url = f"/api/v4/projects/{project_id}/badges"
                    log.debug("going for badges: %s", repr(url))

                    badges = asyncio.run(get_badges(project["id"]))
                    log.debug("badges: %s", repr(badges))

                    project["got_tests"] = bool(
                        asyncio.run(find_badge_item("test_total", badges, project))
                    )
                    project["got_linting"] = bool(
                        asyncio.run(find_badge_item("lint_total", badges, project))
                    )
                    project["got_coverage"] = bool(
                        asyncio.run(find_badge_item("coverage", badges, project))
                    )
                except (
                    gidgetlab.exceptions.BadRequest,
                    gidgetlab.exceptions.GitLabBroken,
                    aiohttp.ClientOSError,
                ) as err:
                    # get a bad request on snippets as it has no code
                    # in repo
                    log.error(
                        "Error processing badges metrics: %s for %s/%s",
                        repr(err),
                        project["id"],
                        project["nameWithNamespace"],
                    )

                # query RTD for existence of docs project
                project_slug = "ska-telescope-" + project["path"].replace(".", "")
                url = f"https://readthedocs.org/api/" f"v3/projects/{project_slug}/"
                log.debug("going for rtd: %s", repr(url))
                rtd = fetchRTD(url)
                log.debug("rtd: %s [%s]", url, rtd)
                if rtd:
                    project["got_rtd"] = True

            dfrow = {k: project[k] for k in PROJECT_KEYS.keys() if k in project}
            projects.append(dfrow)
            log.debug("project added: %s", dfrow)
            project_cnt += 1
            if not project_cnt % 1000:
                log.info("projects: accumulated: %d", project_cnt)

            # now update project history bookmark - every 100 as it is expensive
            project_state[project["id"]] = project
            if project_cnt % 20 == 0:
                save_project_state(project_state)

        # catch the last ones
        save_project_state(project_state)

        log.info("projects: finished collecting: %d", project_cnt)
        projects_df = pandas.DataFrame.from_records(projects)

        projects_df.set_index("gid")
        projects_df["createdAt"] = (
            projects_df["createdAt"].apply(str).apply(pandas.Timestamp)
        )
        projects_df["pi"] = projects_df["createdAt"].apply(which_pi)
        projects_df["sprint"] = projects_df["createdAt"].apply(which_sprint)
        log.info(projects_df)

        if not skipupdate:
            load_table_dataframe(
                projects_df,
                PROJECT_KEYS,
                "jira-reporting-poc.gitlab.gqlprojects",
            )
            log.info("projects: finished loading: %d", project_cnt)
        else:
            log.info("projects: update skipped")

    # MergeRequests
    if not skipmrs:
        mrs = []
        mr_cnt = 0
        cnt = 0
        all_mrs = []

        variables = {"cursor": None}  # Initially, no cursor

        if load:
            d = pathlib.Path(os.path.abspath(DATASETS_DIR))
            log.info("Loading merge requests from: %s", d)
            for filename in d.rglob("merge*.json"):
                log.info("Loading merge request file: %s", filename)
                with open(filename, "r", encoding="utf-8") as f:
                    data = json.load(f)
                    all_mrs += data
        else:
            # loop through each project
            projects = []
            project_cnt = 0
            log.info("Querying projects - to get a list")

            variables = {"cursor": None}  # Initially, no cursor

            # reuse projects
            if not all_projects:
                try:
                    while True:
                        response = fetch_graphql_data(projects_graphql_query, variables)
                        projects = [
                            node["node"]
                            for node in response["data"]["group"]["projects"]["edges"]
                        ]
                        page_info = response["data"]["group"]["projects"]["pageInfo"]
                        all_projects.extend(projects)

                        if page_info["hasNextPage"]:
                            variables["cursor"] = page_info["endCursor"]
                        else:
                            break
                except Exception as e:
                    print("An error occurred:", e)
                    sys.exit(1)

            for project in all_projects:
                project_fullpath = project["fullPath"]
                variables = {"cursor": None}  # Initially, no cursor
                log.info("Querying merge requests")
                response = None
                cnt = 0
                # if len(all_mrs) > 10:
                #     break
                try:
                    while True:
                        cnt += 1
                        created_after = f"{after}T00:00:00+00:00"
                        created_before = f"{before}T23:59:59+00:00"
                        # created_after = f"{after}T23:00:00+00:00"
                        # created_before = f"{before}T23:59:59+00:00"
                        log.info(
                            "Getting a page of merge requests for %s: from %s to %s %d",
                            project_fullpath,
                            created_after,
                            created_before,
                            cnt,
                        )

                        response = fetch_graphql_data(
                            template_mrs_query(
                                mrs_graphql_query,
                                project_fullpath,
                                created_after,
                                created_before,
                            ),
                            variables,
                        )
                        if "data" not in response:
                            log.error("BAD RESPONSE: %s", str(response))
                            log.debug(
                                "REQUEST: %s",
                                template_mrs_query(
                                    mrs_graphql_query,
                                    project_fullpath,
                                    created_after,
                                    created_before,
                                ),
                            )

                        if response["data"]["project"]:
                            node = response["data"]["project"]
                            if node["mergeRequests"]["edges"]:
                                for mr in node["mergeRequests"]["edges"]:
                                    all_mrs.append(
                                        mr["node"]
                                        | {
                                            "project_" + k: node[k]
                                            for k in [
                                                "name",
                                                "path",
                                                "fullPath",
                                                "id",
                                                "createdAt",
                                                "archived",
                                                "description",
                                                "nameWithNamespace",
                                                "visibility",
                                                "repository",
                                            ]
                                        }
                                    )
                        page_info = response["data"]["project"]["mergeRequests"][
                            "pageInfo"
                        ]
                        log.info(
                            "Read %d MRs - total now: %d",
                            len(node["mergeRequests"]["edges"]),
                            len(all_mrs),
                        )

                        if page_info["hasNextPage"]:
                            variables["cursor"] = page_info["endCursor"]
                            log.info("Next cursor: %s", page_info["endCursor"])
                            if not node["mergeRequests"]["edges"]:
                                log.error(
                                    "MRS node is empty even though it says it hasNextPage, we are breaking"
                                )
                                break
                        else:
                            break
                except Exception as e:
                    log.critical("MRS An error occurred: %s/%s", str(type(e)), e)
                    log.debug("RESPONSE: %s", response)
                    # sys.exit(1)

            if dump:
                dump_query_results(
                    f"mergerequests-before{before}-after{after}", all_mrs
                )

        log.info("Processing mrs: %d", len(all_mrs))
        # log.debug("all_mrs: %s", str(all_mrs))
        for mr in all_mrs:
            # skip empty merge requests
            # if not mergeRequest:
            #     continue

            log.debug("mr: %s", str(mr))

            mr["gid"] = mr["id"]
            mr["id"] = int(mr["id"].split("/")[-1])
            mr["iid"] = mr["iid"]
            mr["empty"] = (
                mr["project_repository"]["empty"] if mr["project_repository"] else False
            )
            mr["exists"] = (
                mr["project_repository"]["exists"]
                if mr["project_repository"]
                else False
            )
            mr["default_branch"] = (
                mr["project_repository"]["rootRef"]
                if mr["project_repository"]
                else "Unknown"
            )

            mr["project_gid"] = mr["project_id"]
            mr["project_id"] = int(mr["project_id"].split("/")[-1])
            # mr["project_name"] = mr["project_name"]
            # mr["project_nameWithNamespace"] = mr["project_nameWithNamespace"]
            # mr["project_path"] = mr["project_path"]
            # mr["project_createdAt"] = mr["project_createdAt"]
            # mr["project_archived"] = mr["project_archived"]
            # mr["project_description"] = mr["project_description"]
            # mr["project_visibility"] = mr["project_visibility"]

            mr["author_gid"] = mr["author"]["id"]
            mr["author_id"] = int(mr["author"]["id"].split("/")[-1])
            mr["author_name"] = mr["author"]["name"]
            mr["author_username"] = mr["author"]["username"]

            mr["mergeUser_gid"] = mr["mergeUser"]["id"] if mr["mergeUser"] else ""
            mr["mergeUser_id"] = (
                int(mr["mergeUser"]["id"].split("/")[-1]) if mr["mergeUser"] else 0
            )
            mr["mergeUser_name"] = (
                mr["mergeUser"]["name"] if mr["mergeUser"] else "Unknown"
            )

            mr["additions"] = (
                mr["diffStatsSummary"]["additions"] if mr["diffStatsSummary"] else 0
            )
            mr["changes"] = (
                mr["diffStatsSummary"]["changes"] if mr["diffStatsSummary"] else 0
            )
            mr["deletions"] = (
                mr["diffStatsSummary"]["deletions"] if mr["diffStatsSummary"] else 0
            )

            # for state in STATES:
            #     mr["state_" + state.lower()] = 0

            # mr["pipeline_count"] = 0
            # if mr["pipelines"] and mr["pipelines"]["nodes"]:
            #     mr["pipelines"]["nodes"].sort(key=lambda d: int(d["iid"]))
            #     mr["pipeline_count"] = len(mr["pipelines"]["nodes"])
            #     mr["latest_test_count"] = mr["pipelines"]["nodes"][-1][
            #         "testReportSummary"
            #     ]["total"]["count"]
            #     mr["latest_test_skipped"] = mr["pipelines"]["nodes"][-1][
            #         "testReportSummary"
            #     ]["total"]["skipped"]
            #     mr["latest_test_success"] = mr["pipelines"]["nodes"][-1][
            #         "testReportSummary"
            #     ]["total"]["success"]
            #     mr["latest_test_error"] = mr["pipelines"]["nodes"][-1][
            #         "testReportSummary"
            #     ]["total"]["error"]
            #     mr["latest_test_failed"] = mr["pipelines"]["nodes"][-1][
            #         "testReportSummary"
            #     ]["total"]["failed"]
            #     mr["latest_test_time"] = mr["pipelines"]["nodes"][-1][
            #         "testReportSummary"
            #     ]["total"]["time"]
            #     mr["latest_test_suite_msg"] = mr["pipelines"]["nodes"][-1][
            #         "testReportSummary"
            #     ]["total"]["suiteError"]
            #     mr["latest_pipeline_status"] = mr["pipelines"]["nodes"][-1]["status"]
            #     mr["latest_pipeline_duration"] = mr["pipelines"]["nodes"][-1][
            #         "duration"
            #     ]
            #     mr["latest_pipeline_coverage"] = (
            #         mr["pipelines"]["nodes"][-1]["coverage"]
            #         if mr["pipelines"]["nodes"][-1]["coverage"]
            #         else 0
            #     )
            #     mr["latest_pipeline_startedAt"] = mr["pipelines"]["nodes"][-1][
            #         "startedAt"
            #     ]
            #     mr["latest_pipeline_finishedAt"] = mr["pipelines"]["nodes"][-1][
            #         "finishedAt"
            #     ]
            #     mr["latest_pipeline_finishedAt"] = mr["pipelines"]["nodes"][-1][
            #         "finishedAt"
            #     ]
            #     mr["latest_pipeline_updatedAt"] = mr["pipelines"]["nodes"][-1][
            #         "updatedAt"
            #     ]
            #     mr["latest_pipeline_ref"] = mr["pipelines"]["nodes"][-1]["ref"]
            #     for pipeline in mr["pipelines"]["nodes"]:
            #         mr["state_" + pipeline["status"].lower()] += 1

            # else:
            #     mr["latest_test_count"] = 0
            #     mr["latest_test_skipped"] = 0
            #     mr["latest_test_success"] = 0
            #     mr["latest_test_error"] = 0
            #     mr["latest_test_failed"] = 0
            #     mr["latest_test_time"] = 0
            #     mr["latest_test_suite_msg"] = ""
            #     mr["latest_pipeline_status"] = ""
            #     mr["latest_pipeline_duration"] = 0
            #     mr["latest_pipeline_coverage"] = 0
            #     mr["latest_pipeline_startedAt"] = None
            #     mr["latest_pipeline_finishedAt"] = None
            #     mr["latest_pipeline_updatedAt"] = None
            #     mr["latest_pipeline_ref"] = ""

            dfrow = {k: mr[k] for k in MR_KEYS.keys() if k in mr}
            m = jira_regex.search(str(dfrow["title"]))
            jira_id = ""
            if m:
                jira_id = m.group(1)
            dfrow["jira_id"] = jira_id.upper()
            mrs.append(dfrow)
            mr_cnt += 1
            if not mr_cnt % 1000:
                log.info("mrs: accumulated: %d", mr_cnt)

        log.info("mrs: finished collecting: %d", mr_cnt)
        mrs_df = pandas.DataFrame.from_records(mrs)

        if all_mrs:
            mrs_df.set_index("gid")
            mrs_df["createdAt"] = mrs_df["createdAt"].apply(str).apply(pandas.Timestamp)
            log.info("createdAt : %s", str(mrs_df["createdAt"]))
            mrs_df["pi"] = mrs_df["createdAt"].apply(which_pi)
            mrs_df["sprint"] = mrs_df["createdAt"].apply(which_sprint)

        log.info(mrs_df)
        log.info(mrs_df.columns)
        if not skipupdate:
            load_table_dataframe(
                mrs_df,
                MR_KEYS,
                "jira-reporting-poc.gitlab.gqlmrsbyproject",
            )
            log.info("mrs: finished loading: %d", mr_cnt)
        else:
            log.info("mrs: update skipped")

    # Pipelines
    if not skippipelines:
        pipelines = []
        pipeline_cnt = 0
        cnt = 0
        all_pipelines = []

        variables = {"cursor": None}  # Initially, no cursor

        if load:
            d = pathlib.Path(os.path.abspath(DATASETS_DIR))
            log.info("Loading pipelines from: %s", d)
            for filename in d.rglob("pipeline*.json"):
                log.info("Loading pipeline request file: %s", filename)
                with open(filename, "r", encoding="utf-8") as f:
                    data = json.load(f)
                    all_pipelines += data
        else:
            log.info("Querying pipelines")

            # loop through each project
            projects = []
            project_cnt = 0
            log.info("Querying projects - to get a list")

            variables = {"cursor": None}  # Initially, no cursor

            # reuse projects
            if not all_projects:
                try:
                    while True:
                        response = fetch_graphql_data(projects_graphql_query, variables)
                        projects = [
                            node["node"]
                            for node in response["data"]["group"]["projects"]["edges"]
                        ]
                        page_info = response["data"]["group"]["projects"]["pageInfo"]
                        all_projects.extend(projects)

                        if page_info["hasNextPage"]:
                            variables["cursor"] = page_info["endCursor"]
                        else:
                            break
                except Exception as e:
                    print("An error occurred:", e)
                    sys.exit(1)

            log.info("Projects to process: %d", len(all_projects))
            for project in all_projects:
                project_fullpath = project["fullPath"]
                variables = {"cursor": None}  # Initially, no cursor

                # if not project_fullpath == "ska-telescope/ska-tango-images":
                #     continue

                # if len(all_pipelines) > 10:
                #     break

                try:
                    while True:
                        cnt += 1
                        created_after = f"{after}T00:00:00+00:00"
                        created_before = f"{before}T23:59:59+00:00"
                        # created_after = f"{after}T23:00:00+00:00"
                        # created_before = f"{before}T23:59:59+00:00"
                        log.info(
                            "Getting a page of pipelines for %s: from %s to %s %d",
                            project_fullpath,
                            created_after,
                            created_before,
                            cnt,
                        )
                        response = fetch_graphql_data(
                            template_pipelines_query(
                                pipelines_graphql_query,
                                project_fullpath,
                                created_after,
                                created_before,
                            ),
                            variables,
                        )
                        if "data" not in response:
                            log.error("BAD RESPONSE: %s", str(response))
                            log.debug(
                                "REQUEST: %s",
                                template_pipelines_query(
                                    pipelines_graphql_query,
                                    project_fullpath,
                                    created_after,
                                    created_before,
                                ),
                            )
                        if response["data"]["project"]["pipelines"]:
                            pipelines = [
                                {"pipeline": node["node"]}
                                | {
                                    k: response["data"]["project"][k]
                                    for k in [
                                        "name",
                                        "path",
                                        "fullPath",
                                        "id",
                                        "createdAt",
                                        "archived",
                                        "description",
                                        "nameWithNamespace",
                                        "visibility",
                                        "repository",
                                    ]
                                }
                                for node in response["data"]["project"]["pipelines"][
                                    "edges"
                                ]
                            ]
                            all_pipelines.extend(pipelines)
                        page_info = response["data"]["project"]["pipelines"]["pageInfo"]
                        # log.info("This lot of pipelines: %s", [pipeline["pipelines"] for pipeline in pipelines])
                        log.info(
                            "Read %d pipelines - total now: %d",
                            len(pipelines),
                            len(all_pipelines),
                        )

                        if page_info["hasNextPage"]:
                            variables["cursor"] = page_info["endCursor"]
                            log.info("Next cursor: %s", page_info["endCursor"])
                            if not pipelines:
                                log.error(
                                    "pipelines node is empty even though it says it hasNextPage, we are breaking"
                                )
                                break
                            # if len(all_pipelines) > 1000:
                            #     break
                            # else:
                            #     break
                        else:
                            break
                except Exception as e:
                    log.info("%s", response)
                    log.critical("pipelines An error occurred: %s/%s", str(type(e)), e)
                    sys.exit(1)

            if dump:
                dump_query_results(
                    f"pipelines-before{before}-after{after}", all_pipelines
                )

        log.info("Processing pipelines: %d", len(all_pipelines))
        # log.debug("all_pipelines: %s", str(all_pipelines))
        for pipeline in all_pipelines:
            log.debug("pipeline: %s", str(pipeline))

            # {'id': 'gid://gitlab/Ci::Pipeline/1057430507', 'iid': '21', 'status': 'SUCCESS', 'duration': 1332, 'coverage': None, 'startedAt': '2023-11-01T09:57:45Z', 'finishedAt': '2023-11-01T10:20:12Z', 'updatedAt': '2023-11-01T10:20:13Z', 'ref': '0.0.1', 'testReportSummary': {'total': {'count': 0, 'skipped': 0, 'success': 0, 'error': 0, 'failed': 0, 'time': 0.0, 'suiteError': None}}, 'mergeRequest': None}

            # {'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/1057430507', 'iid': '21', 'status': 'SUCCESS', 'duration': 1332, 'coverage': None, 'startedAt': '2023-11-01T09:57:45Z', 'finishedAt': '2023-11-01T10:20:12Z', 'updatedAt': '2023-11-01T10:20:13Z', 'ref': '0.0.1', 'testReportSummary': {'total': {'count': 0, 'skipped': 0, 'success': 0, 'error': 0, 'failed': 0, 'time': 0.0, 'suiteError': None}}, 'mergeRequest': None}, 'name': 'Ska GUI Local Storage', 'path': 'ska-gui-local-storage', 'fullPath': 'ska-telescope/ska-gui-local-storage', 'id': 'gid://gitlab/Project/51308533', 'createdAt': '2023-10-17T10:38:20Z', 'archived': False, 'description': 'Services that allow for the access and saving of information to/from local storage', 'nameWithNamespace': 'SKAO / Ska GUI Local Storage', 'visibility': 'public', 'repository': {'empty': False, 'exists': True, 'rootRef': 'main'}}

            # shuffle project ids
            pipeline["project_gid"] = pipeline["id"]
            pipeline["project_id"] = int(pipeline["id"].split("/")[-1])
            pipeline["projectId"] = pipeline["project_id"]
            pipeline["project_name"] = pipeline["name"]
            pipeline["project_nameWithNamespace"] = pipeline["nameWithNamespace"]
            pipeline["project_path"] = pipeline["path"]
            pipeline["project_createdAt"] = pipeline["createdAt"]
            pipeline["project_archived"] = pipeline["archived"]
            pipeline["project_description"] = pipeline["description"]
            pipeline["project_visibility"] = pipeline["visibility"]

            # shuffle pipeline values
            pipeline["gid"] = pipeline["pipeline"]["id"]
            pipeline["id"] = int(pipeline["pipeline"]["id"].split("/")[-1])
            pipeline["iid"] = pipeline["pipeline"]["iid"]
            pipeline["empty"] = (
                pipeline["repository"]["empty"] if pipeline["repository"] else False
            )
            pipeline["exists"] = (
                pipeline["repository"]["exists"] if pipeline["repository"] else False
            )
            pipeline["default_branch"] = (
                pipeline["repository"]["rootRef"]
                if pipeline["repository"]
                else "Unknown"
            )

            if pipeline["pipeline"]["testReportSummary"]:
                pipeline["test_count"] = pipeline["pipeline"]["testReportSummary"][
                    "total"
                ]["count"]
                pipeline["test_skipped"] = pipeline["pipeline"]["testReportSummary"][
                    "total"
                ]["skipped"]
                pipeline["test_success"] = pipeline["pipeline"]["testReportSummary"][
                    "total"
                ]["success"]
                pipeline["test_error"] = pipeline["pipeline"]["testReportSummary"][
                    "total"
                ]["error"]
                pipeline["test_failed"] = pipeline["pipeline"]["testReportSummary"][
                    "total"
                ]["failed"]
                pipeline["test_time"] = pipeline["pipeline"]["testReportSummary"][
                    "total"
                ]["time"]
                pipeline["test_suite_msg"] = pipeline["pipeline"]["testReportSummary"][
                    "total"
                ]["suiteError"]
                pipeline["pipeline_status"] = pipeline["pipeline"]["status"]
                pipeline["pipeline_duration"] = pipeline["pipeline"]["duration"]
                pipeline["pipeline_coverage"] = (
                    pipeline["pipeline"]["coverage"]
                    if pipeline["pipeline"]["coverage"]
                    else 0
                )
                if pipeline["pipeline"]["startedAt"]:
                    pipeline["pipeline_startedAt"] = pipeline["pipeline"]["startedAt"]
                else:
                    pipeline["pipeline_startedAt"] = "1970-01-01T00:00:00Z"
                if pipeline["pipeline"]["finishedAt"]:
                    pipeline["pipeline_finishedAt"] = pipeline["pipeline"]["finishedAt"]
                else:
                    pipeline["pipeline_finishedAt"] = "1970-01-01T00:00:00Z"
                if pipeline["pipeline"]["updatedAt"]:
                    pipeline["pipeline_updatedAt"] = pipeline["pipeline"]["updatedAt"]
                else:
                    pipeline["pipeline_updatedAt"] = "1970-01-01T00:00:00Z"
                pipeline["pipeline_ref"] = pipeline["pipeline"]["ref"]

            else:
                pipeline["test_skipped"] = 0
                pipeline["test_success"] = 0
                pipeline["test_error"] = 0
                pipeline["test_failed"] = 0
                pipeline["test_time"] = 0
                pipeline["test_suite_msg"] = ""
                pipeline["pipeline_status"] = ""
                pipeline["pipeline_duration"] = 0
                pipeline["pipeline_coverage"] = 0
                pipeline["pipeline_startedAt"] = "1970-01-01T00:00:00Z"
                pipeline["pipeline_finishedAt"] = "1970-01-01T00:00:00Z"
                pipeline["pipeline_updatedAt"] = "1970-01-01T00:00:00Z"
                pipeline["pipeline_ref"] = ""

            if pipeline["pipeline"]["mergeRequest"]:
                for k in [
                    "description",
                    "title",
                    "sourceBranch",
                    "targetBranch",
                    "state",
                    "approved",
                ]:
                    pipeline[k] = pipeline["pipeline"]["mergeRequest"][k]
                for k in ["createdAt", "updatedAt", "mergedAt"]:
                    pipeline[k] = (
                        pipeline["pipeline"]["mergeRequest"][k]
                        if pipeline["pipeline"]["mergeRequest"][k]
                        else "1970-01-01T00:00:00Z"
                    )
            else:
                for k in [
                    "description",
                    "title",
                    "sourceBranch",
                    "targetBranch",
                    "state",
                ]:
                    pipeline[k] = ""
                for k in ["createdAt", "updatedAt", "mergedAt"]:
                    pipeline[k] = "1970-01-01T00:00:00Z"
                pipeline["approved"] = False

            pipeline["mr_id"] = (
                int(pipeline["pipeline"]["mergeRequest"]["id"].split("/")[-1])
                if pipeline["pipeline"]["mergeRequest"]
                else 0
            )

            # for state in STATES:
            #     pipeline["state_" + state.lower()] = 0

            # pipeline["pipeline_count"] = 0

            dfrow = {k: pipeline[k] for k in PIPELINE_KEYS.keys() if k in pipeline}
            log.debug("pipelines: dfrow: %s", dfrow)

            m = jira_regex.search(str(dfrow["title"]))
            jira_id = ""
            if m:
                jira_id = m.group(1)
            dfrow["jira_id"] = jira_id.upper()
            pipelines.append(dfrow)
            pipeline_cnt += 1
            if not pipeline_cnt % 1000:
                log.info("pipelines: accumulated: %d", pipeline_cnt)

        log.info("pipelines: finished collecting: %d", pipeline_cnt)
        pipelines_df = pandas.DataFrame.from_records(pipelines)

        log.info("pipelines: finished collecting: %s", pipelines_df)

        if all_pipelines:
            pipelines_df.set_index("gid")

            log.info("startedAt : %s", str(pipelines_df["pipeline_startedAt"]))

            pipelines_df["pipeline_startedAt"] = (
                pipelines_df["pipeline_startedAt"].apply(str).apply(pandas.Timestamp)
            )
            pipelines_df["pipeline_finishedAt"] = (
                pipelines_df["pipeline_finishedAt"].apply(str).apply(pandas.Timestamp)
            )
            pipelines_df["pipeline_updatedAt"] = (
                pipelines_df["pipeline_updatedAt"].apply(str).apply(pandas.Timestamp)
            )
            pipelines_df["mergedAt"] = (
                pipelines_df["mergedAt"].apply(str).apply(pandas.Timestamp)
            )
            pipelines_df["project_createdAt"] = (
                pipelines_df["project_createdAt"].apply(str).apply(pandas.Timestamp)
            )
            # pipelines_df["createdAt"] = pipelines_df["createdAt"].apply(str).apply(pandas.Timestamp)
            # log.info("createdAt : %s", str(pipelines_df["createdAt"]))
            pipelines_df["pi"] = pipelines_df["pipeline_startedAt"].apply(which_pi)
            pipelines_df["sprint"] = pipelines_df["pipeline_startedAt"].apply(
                which_sprint
            )

        log.info(pipelines_df)
        if not skipupdate:
            load_table_dataframe(
                pipelines_df,
                PIPELINE_KEYS,
                "jira-reporting-poc.gitlab.gqlpipelinebyprojects",
            )
            log.info("pipelines: finished loading: %d", pipeline_cnt)
        else:
            log.info("pipelines: update skipped")

    # all done.
    log.info("all done.")
    sys.exit()


if __name__ == "__main__":
    main()
