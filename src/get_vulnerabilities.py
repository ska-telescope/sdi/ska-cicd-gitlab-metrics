#!/usr/bin/env python

import asyncio
import json
import logging
import os
import sys

import aiohttp
import gidgetlab.aiohttp

GITLAB_URL = "https://gitlab.com/"
GITLAB_USER = "ska-telescope"
# GITLAB_USER = "piersharding"
PER_PAGE = 1000
project_name = "piersharding/dask-operator"
branch_name = "master"

gl_token = os.getenv("GITLAB_TOKEN")
log = logging.getLogger("gitlab_metrics")

# https://gitlab.com/api/v4/projects/19278002


async def main_projects():
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        # Make your requests, e.g. ...
        url = "{base_url}api/v4/groups/{gitlab_user}/projects?simple=true&license=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        return data


async def sub_group_projects(subgroup):
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        sgres = []
        try:
            thispage = 0
            per_page = 100
            call_counter = 0
            url = "{base_url}api/v4/groups/{sub_group:d}/projects?simple=false&license=true&per_page={per_page:d}".format(
                base_url=GITLAB_URL, sub_group=subgroup, per_page=PER_PAGE
            )
            while True:
                # Keep going until results less than a page
                thispage += 1
                this_url = f"{url}&per_page={per_page:d}&page={thispage:d}"
                data = await gl.getitem(this_url)
                log.info("[call_api] received: %d", len(data))
                call_counter += 1
                # save results and check to go round again
                sgres.extend(data)
                if len(data) < per_page:
                    break
        except aiohttp.client_exceptions.ClientConnectorError as err:
            log.error("[call_api] exception: %s", str(err))
        except gidgetlab.exceptions.GitLabBroken as err:
            log.error("[call_api] exception: %s", str(err))
        return sgres


async def sub_groups():
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        # Make your requests, e.g. ...
        url = "{base_url}api/v4/groups/{gitlab_user}/subgroups?simple=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        subgroups = [g["id"] for g in data]
        return subgroups


async def main_project(project_id, branch):
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        url = (
            "{base_url}api/v4/projects/{project_id}"
            "/pipelines?simple=true&per_page={per_page:d}"
            "&page={page:d}&order_by=id&sort=desc"
            "&ref={branch}".format(
                base_url=GITLAB_URL,
                project_id=project_id,
                per_page=1,
                page=1,
                branch=branch,
            )
        )
        data = await gl.getitem(url)
        return data


async def main():
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        url = "{base_url}api/v4/groups/{gitlab_user}/projects?simple=true&license=true&per_page={per_page:d}".format(
            base_url=GITLAB_URL, gitlab_user=GITLAB_USER, per_page=PER_PAGE
        )
        data = await gl.getitem(url)
        return data


async def get_project(project):
    async with aiohttp.ClientSession() as session:
        gl = gidgetlab.aiohttp.GitLabAPI(session, GITLAB_USER, access_token=gl_token)
        url = "{base_url}api/v4/projects/{project:d}?license=true".format(
            base_url=GITLAB_URL, project=project
        )
        data = await gl.getitem(url)
        return data


async def get_vulnerabilities(project):
    try:
        async with aiohttp.ClientSession() as session:
            gl = gidgetlab.aiohttp.GitLabAPI(
                session, GITLAB_USER, access_token=gl_token
            )
            # Make your requests, e.g. ...
            url = "{base_url}api/v4/projects/{project:d}/vulnerability_findings?report_type=container_scanning&scope=all&per_page=100".format(
                base_url=GITLAB_URL, project=project
            )
            data = await gl.getitem(url)
        return data
    except (
        gidgetlab.exceptions.BadRequest,
        gidgetlab.exceptions.GitLabBroken,
        aiohttp.ClientOSError,
    ) as err:
        log.debug("[get_vulnerabilities] failed: %s", err)
        return []


# data = asyncio.run(sub_groups())
# data = asyncio.run(main())  # cluster-k8s
# data = asyncio.run(main_project(16996947, "master"))  # cluster-k8s
# data = asyncio.run(main_projects())
groups = asyncio.run(sub_groups())
# print(data)
print("SubGroups: {}".format(len(groups)))
projects = []
# 3180705 - ska-telescope group
groups.append(3180705)
for group in groups:
    print("Group: {}".format(group))
    projs = asyncio.run(sub_group_projects(group))
    projects.extend(projs)

print("Total Projects: {}".format(len(projects)))

rows = []
for p in projects:
    proj = asyncio.run(get_project(p["id"]))
    # print(proj)
    text = ""
    author_email = ""
    author_name = ""
    if not p["path_with_namespace"] == "ska-telescope/sdp/ska-sdp-proccontrol":
        continue
    res = asyncio.run(get_vulnerabilities(p["id"]))
    print(json.dumps(res))
    vulns = {}
    for v in res:
        if v["report_type"] == "container_scanning":
            if not v["severity"] in vulns:
                vulns[v["severity"]] = 0
            vulns[v["severity"]] += 1
    if vulns:
        print("id: {} slug: {}".format(p["id"], p["path_with_namespace"]))
        print(vulns)
        # sys.exit(0)
    # if 'commit' in res:
    #     author_email = res['commit']['author_email']
    #     author_name = res['commit']['author_name']

    # d = [p['id'], p['path_with_namespace'], ('Yes' if p['archived'] else 'No'), default_branch, p['web_url'], license_url, author_email, author_name]
    # key = ""
    # nickname = ""
    # name = ""
    # if proj['license']:
    #     key = proj['license']['key']
    #     nickname = proj['license']['nickname']
    #     name = proj['license']['name']
    # d.extend([key, nickname, name, text])
    # rows.append(d)

sys.exit(0)
